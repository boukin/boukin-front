import type { UserRegistration } from "~/types/authentication";

export const useRegisterForm = () => {
  /** Checks if the password and the confirmation are the same and if it passes the regex
   * @param password1 : password
   * @param password2 : passwordConfirmation
   * @returns boolean
   */
  const validatePassword = (password1: string, password2: string) => {
    /**
     * At least one lowercase letter ((?=.*[a-z]))
     * At least one uppercase letter ((?=.*[A-Z]))
     * At least one digit ((?=.*\d))
     * At least one special character among @$!%*?& ((?=.*[@$!%*?&]))
     * Length between 8 and 20 ({8,20})
     */
    const passwordRegex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/;
    const isValid = passwordRegex.test(password1);
    if (password1 === password2) return isValid;
    else return false;
  };

  /** Checks if every fields of the form are completed
   * @param form : user registraition form
   * @returns boolean
   */
  const validateRegisterForm = (form: UserRegistration) => {
    const requiredKeys: (keyof UserRegistration)[] = [
      "username",
      "email",
      "password",
      "passwordConfirmation",
    ];
    return requiredKeys.every((key) => form[key] && form[key].length > 0);
  };

  return {
    validatePassword,
    validateRegisterForm,
  };
};
