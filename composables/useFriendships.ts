import type { Friendship } from "~/types/friendship";
import type { AuthenticationToken } from "~/types/authentication";

export const fetchFriendships = async () => {
  const config = useRuntimeConfig();

  const friendships = useState<Friendship[]>("friendships", () => []);
  const error = ref();

  const userCookie = useCookie<AuthenticationToken>("userCookie");
  const friendshipApi = `/users/${userCookie.value.user_id}/friendships/`;

  const { data, error: _error } = await useFetch(friendshipApi, {
    baseURL: config.public.apiBoukinBaseUrl,
    method: "GET",
    headers: {
      Authorization: `Token ${userCookie.value.token}`,
    },
  });
  if (_error.value) {
    error.value = _error.value;
    console.error(error.value);
  } else if (data.value) {
    friendships.value = (data.value as Friendship[]) ?? [];
  }

  return { friendships, error };
};
