import type {
  UserAuthentication,
  AuthenticationToken,
} from "~/types/authentication";
/** Prend en entrée l'email et le mot de passe et crée / met à jour le cookie
 * @param email : email de l'utilisateur
 * @param password: mot de passe de l'utilisateur
 */

export const useAuthCookie = async (
  email: UserAuthentication["email"],
  password: UserAuthentication["password"],
) => {
  const config = useRuntimeConfig();

  const response = await useFetch("/auth-token/", {
    baseURL: config.public.apiBoukinBaseUrl,
    method: "POST",
    body: {
      email: email,
      password: password,
    },
    onRequestError({ error }) {
      console.error(error);
    },
    onResponse({ response }) {
      if (response.status == 200) {
        const userCookie = useCookie("userCookie", {
          maxAge: 60 * 60 * 24 * 30,
        });
        if (userCookie.value && userCookie.value["email"] == email) {
          return;
        } else {
          userCookie.value = response._data;
        }
      } else if (response.status == 400) {
        console.error(`${response.status} ${response.statusText}`);
        throw new Error("Impossible de s'authentifier.");
      }
    },
  });
  return response;
};
