export const useForm = () => {
  /** Removes fields that have empty string content
   * @param formData : form containing empty fields
   * @returns form without the empty fields
   */
  const cleanForm = (formData: { [key: string]: string }) => {
    return Object.fromEntries(
      Object.entries(formData).filter(([_, value]) => value.trim() !== ""),
    );
  };

  return {
    cleanForm,
  };
};
