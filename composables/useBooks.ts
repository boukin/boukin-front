import type { AuthenticationToken } from "~/types/authentication";
import type { Book, BookCreation } from "~/types/boukinApi";

export const postOwnedBook = (book_id: string) => {
  const config = useRuntimeConfig();
  const userCookie = useCookie<AuthenticationToken>("userCookie");

  return useFetch("/owned-books/", {
    baseURL: config.public.apiBoukinBaseUrl,
    method: "POST",
    headers: {
      Authorization: `Token ${userCookie.value.token}`,
    },
    body: {
      owner: userCookie.value.user_id,
      book: book_id,
      status: "available",
    },
  });
};

export const postBook = (bookData: BookCreation) => {
  const config = useRuntimeConfig();
  const userCookie = useCookie<AuthenticationToken>("userCookie");

  return useFetch<Book>("/books/", {
    baseURL: config.public.apiBoukinBaseUrl,
    method: "POST",
    headers: {
      Authorization: `Token ${userCookie.value.token}`,
    },
    body: {
      title: bookData["title"],
      author_fname: bookData["author_fname"],
      author_lname: bookData["author_lname"],
      isbn: bookData["isbn"],
    },
  });
};
