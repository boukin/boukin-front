import type { AuthenticationToken } from "~/types/authentication";
import type { Loan } from "~/types/loan";

interface LoansBody {
  total_number_of_loans: Number;
  loans: Loan[];
}

export const fetchLoans = async () => {
  const userCookie = useCookie<AuthenticationToken>("userCookie");
  const config = useRuntimeConfig();
  const loansEndpoint = `/users/${userCookie.value.user_id}/loans/`;
  const error = ref();
  const { data, error: _error } = await useFetch(loansEndpoint, {
    baseURL: config.public.apiBoukinBaseUrl,
    headers: {
      Authorization: `Token ${userCookie.value.token}`,
    },
  });
  if (data.value) {
    const totalNumberOfLoans = (data.value as LoansBody).total_number_of_loans;
    const loans = (data.value as LoansBody).loans;
    return { totalNumberOfLoans, loans };
  } else {
    error.value = _error.value;
    return { error };
  }
};
