import { defineVitestConfig } from "@nuxt/test-utils/config";

export default defineVitestConfig({
  // any custom Vitest config you require
  test: {
    poolOptions: {
      threads: {
        isolate: true,
      },
    },
    coverage: {
      provider: "v8",
      // ajouter "app.vue", "layouts/**" et "middleware/**" ?
      include: ["components/**", "composables/**", "pages/**"],
      reporter: ["text", "html", "cobertura"],
    },
  },
});
