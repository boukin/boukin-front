# Boukin Front

## Installer et exécuter le projet dans un contexte de développement

### Avec Docker (recommandé)

```
git clone git@gitlab.com:boukin/boukin-back.git
git clone git@gitlab.com:boukin/boukin-front.git
cd boukin-back
docker compose --profile dev up
```

### Sans Docker

Nécessite l'installation de **npm**.

```
npm install
npm run dev
```

Quelque soit la configuration initiale, installation du _linter_ (**eslint**) et du _formatter_ (**prettier**) (nécessite Python et **pip**) :

```
pip install pre-commit
pre-commit install
```

## Exécuter les tests

### Avec Docker

```
docker exec -ti boukin-front-dev-1 npm test
```

### Sans Docker

```
npm test
```
