#!/bin/bash

set -e

cd .. 
git checkout main
git pull
cd ../boukin-back
docker compose up --no-deps --build front-prod
