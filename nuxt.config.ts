// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ["~/assets/css/style.css"],
  app: {
    head: {
      title: "Boukin",
      htmlAttrs: { lang: "fr" },
      meta: [
        {
          name: "description",
          content: "Boukin, l'application de prêts de livres entre ami-e-s",
        },
        { name: "keywords", content: "Books, Book loans" },
        { name: "author", content: "Boukin team" },
      ],
      link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    },
  },
  $development: {
    runtimeConfig: {
      public: {
        apiBoukinBaseUrl:
          process.env.API_BOUKIN_BASE_URL || "http://host.docker.internal:8001",
      },
    },
  },
  $production: {
    runtimeConfig: {
      public: {
        apiBoukinBaseUrl: "https://api.boukin.adaschool.fr",
      },
    },
  },
  $test: {
    runtimeConfig: {
      public: {
        apiBoukinBaseUrl: "",
      },
    },
    experimental: {
      appManifest: false,
    },
  },
  devtools: { enabled: true },
  modules: [
    "@nuxt/test-utils/module",
    [
      "nuxt-modernizr",
      {
        "feature-detects": ["test/img/webp"],
        minify: true,
        options: ["setClasses"],
      },
    ],
  ],
  compatibilityDate: "2024-07-05",
});
