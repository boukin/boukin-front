import type { AuthenticationToken } from "~/types/authentication";

export default defineNuxtRouteMiddleware((to, from) => {
  const userCookie = useCookie<AuthenticationToken>("userCookie");
  if (!userCookie.value) {
    return navigateTo("/register");
  }
});
