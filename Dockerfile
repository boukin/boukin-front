ARG PORT=3000
FROM node:lts-slim
WORKDIR /boukin-front
COPY package.json package-lock.json ./
RUN npm install && npm cache clean --force
COPY . .
EXPOSE $PORT
CMD ["npm", "run", "dev"]
