import NewPage from "../pages/loans/new.vue";
import type { Friendship } from "~/types/friendship";
import type { OwnedBook } from "~/types/boukinApi";
import { afterEach, beforeEach, expect, describe, it, vi } from "vitest";
import { cleanup, screen, within } from "@testing-library/vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import userEvent from "@testing-library/user-event";
import * as matchers from "@testing-library/jest-dom/matchers";

expect.extend(matchers);
const userId = "3c48ecd4-d0bd-4171-a034-4444b768fbd8";
const token = "9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b";
const today = new Date();
const one_week_later = new Date();
one_week_later.setDate(today.getDate() + 7);
const today_iso8601 = today.toISOString().substring(0, 10);
const one_week_later_iso8601 = one_week_later.toISOString().substring(0, 10);

describe("new loan page static elements test suite", () => {
  beforeEach(async () => {
    await renderSuspended(NewPage);
  });
  afterEach(() => cleanup());
  it("displays a heading and a link showing the page hierarchy concerning the loans website section", () => {
    const myLoansLink = screen.getByRole("link", { name: "Mes prêts" });
    const newLoanPageHeading = screen.getByRole("heading", {
      name: /Nouveau prêt/i,
    });
    expect(myLoansLink).toBeVisible();
    expect(newLoanPageHeading).toBeVisible();
  });
  it("displays a heading describing the purpose of the form of creating a new loan", () => {
    const newLoanHeading = screen.getByRole("heading", {
      name: "Prêter un livre à un-e ami-e",
    });
    expect(newLoanHeading).toBeVisible();
  });
  it("displays a form for the loan creation", () => {
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    expect(loanCreationForm).toBeVisible();
  });
  it("displays a selection of available books to be loaned to a friend", () => {
    const bookSelection = screen.getByRole("combobox", {
      name: /Sélectionner un livre/i,
    });
    expect(bookSelection).toBeVisible();
  });
  it("displays a selection of friends to loan books to", () => {
    const friendSelection = screen.getByRole("combobox", {
      name: /Sélectionner un-e ami-e/i,
    });
    expect(friendSelection).toBeVisible();
  });
  it("displays a start date selection for the loan to be created", () => {
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    const loanStartDateSelection = within(loanCreationForm).getByLabelText(
      "Date de début du prêt",
    );
    expect(loanStartDateSelection).toBeVisible();
  });
  it("displays an end date selection for the loan to be created", () => {
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    const loanEndDateSelection = within(loanCreationForm).getByLabelText(
      "Date de fin du prêt",
    );
    expect(loanEndDateSelection).toBeVisible();
  });
  it("displays a button to validate the creation of a new loan", () => {
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    const loanCreationButton = within(loanCreationForm).getByRole("button", {
      name: "Créer le prêt",
    });
    expect(loanCreationButton).toBeVisible();
  });
});

describe("new loan page dynamic elements test suite", () => {
  const userFriendships: Friendship[] = require("./fixtures/friendships.json");
  const userOwnedBooks = require("./fixtures/books/multipleownedbooks.json");
  beforeEach(async () => {
    await renderSuspended(NewPage);
  });
  afterEach(() => cleanup());
  const mocks = vi.hoisted(() => {
    return {
      getUserFriends: vi.fn(),
      getOwnedBooks: vi.fn(),
      postLoan: vi.fn(),
    };
  });
  registerEndpoint(`/users/${userId}/friendships/`, mocks.getUserFriends);
  registerEndpoint(`/owned-books/`, mocks.getOwnedBooks);
  registerEndpoint(`/loans/`, mocks.postLoan);
  mockNuxtImport("useCookie", () => {
    return () => {
      return {
        value: {
          email: "wowilovebooks@boukin.org",
          user_id: userId,
          token: token,
        },
      };
    };
  });
  mocks.getUserFriends.mockReturnValue(userFriendships);
  mocks.getOwnedBooks.mockReturnValue(userOwnedBooks);

  it("lists all friends to select a friend to loan a book to", async () => {
    const friendsUsernames: string[] = [];
    const userActiveFriendships = userFriendships.filter(
      (friendship) => friendship.status === "friendship_active",
    );
    userActiveFriendships.forEach(function (friendship: Friendship) {
      friendsUsernames.push(friendship["friend"]["username"]);
    });
    const friendSelection = screen.getByRole("combobox", {
      name: /Sélectionner un-e ami-e/i,
    });
    const { getAllByRole } = within(friendSelection);
    const friendOptions = getAllByRole("option");
    for (let index = 1; index < friendsUsernames.length; index++) {
      expect(friendOptions[index]).toHaveTextContent(
        friendsUsernames[index - 1],
      );
    }
  });
  it("lists all available books owned by the user to select a book to loan", async () => {
    const ownedBooksTitles: string[] = [];
    userOwnedBooks.forEach(function (ownedBook: OwnedBook) {
      ownedBooksTitles.push(ownedBook["book"]["title"]);
    });
    const bookSelection = screen.getByRole("combobox", {
      name: /Sélectionner un livre/i,
    });
    const { getAllByRole } = within(bookSelection);
    const bookOptions = getAllByRole("option");
    const loanedBookTitle = screen.queryByText(ownedBooksTitles[1]);
    const notVisibleBookTitle = screen.queryByText(ownedBooksTitles[2]);
    expect(bookOptions[1]).toHaveTextContent(ownedBooksTitles[0]);
    expect(loanedBookTitle).not.toBeInTheDocument();
    expect(notVisibleBookTitle).not.toBeInTheDocument();
  });
  it("displays a confirmation message after selecting a friend, a book and inputing dates, then clicking on the loan creation button and sending the loan information", async () => {
    const mockResponse = new Response(null, { status: 201 });
    mocks.postLoan.mockReturnValue(mockResponse);
    const user = userEvent.setup();
    const bookSelection = screen.getByRole("combobox", {
      name: /Sélectionner un livre/i,
    });
    const bookOptions = within(bookSelection).getAllByRole("option");
    const friendSelection = screen.getByRole("combobox", {
      name: /Sélectionner un-e ami-e/i,
    });
    const friendOptions = within(friendSelection).getAllByRole("option");
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    const loanCreationButton = within(loanCreationForm).getByRole("button", {
      name: "Créer le prêt",
    });
    const loanStartDateSelection = within(loanCreationForm).getByLabelText(
      "Date de début du prêt",
    );
    const loanEndDateSelection = within(loanCreationForm).getByLabelText(
      "Date de fin du prêt",
    );
    await user.selectOptions(bookSelection, bookOptions[1]);
    await user.selectOptions(friendSelection, friendOptions[1]);
    await user.clear(loanStartDateSelection);
    await user.type(loanStartDateSelection, today_iso8601);
    await user.clear(loanEndDateSelection);
    await user.type(loanEndDateSelection, one_week_later_iso8601);
    await user.click(loanCreationButton);
    const confirmationMessage = screen.getByText("Le prêt a bien été créé !");
    expect(confirmationMessage).toBeVisible();
  });
  it("displays a message saying to the user that all fields must be filled to create a new loan", async () => {
    const user = userEvent.setup();
    const friendSelection = screen.getByRole("combobox", {
      name: /Sélectionner un-e ami-e/i,
    });
    const friendOptions = within(friendSelection).getAllByRole("option");
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    const loanCreationButton = within(loanCreationForm).getByRole("button", {
      name: "Créer le prêt",
    });
    const loanStartDateSelection = within(loanCreationForm).getByLabelText(
      "Date de début du prêt",
    );
    const loanEndDateSelection = within(loanCreationForm).getByLabelText(
      "Date de fin du prêt",
    );
    await user.click(friendOptions[1]);
    await user.clear(loanStartDateSelection);
    await user.type(loanStartDateSelection, today_iso8601);
    await user.clear(loanEndDateSelection);
    await user.type(loanEndDateSelection, one_week_later_iso8601);
    await user.click(loanCreationButton);
    const fillFieldsErrorMessage = screen.getByText(
      "Création de prêt impossible : tous les champs sont obligatoires pour la création d'un prêt !",
    );
    expect(fillFieldsErrorMessage).toBeVisible();
  });
  it("displays an error message if the loan creation request has failed on the server side", async () => {
    const mockResponse = new Response(null, { status: 404 });
    mocks.postLoan.mockResolvedValue(mockResponse);
    const user = userEvent.setup();
    const bookSelection = screen.getByRole("combobox", {
      name: /Sélectionner un livre/i,
    });
    const bookOptions = within(bookSelection).getAllByRole("option");
    const friendSelection = screen.getByRole("combobox", {
      name: /Sélectionner un-e ami-e/i,
    });
    const friendOptions = within(friendSelection).getAllByRole("option");
    const loanCreationForm = screen.getByRole("form", {
      name: "Création d'un prêt",
    });
    const loanCreationButton = within(loanCreationForm).getByRole("button", {
      name: "Créer le prêt",
    });
    const loanStartDateSelection = within(loanCreationForm).getByLabelText(
      "Date de début du prêt",
    );
    const loanEndDateSelection = within(loanCreationForm).getByLabelText(
      "Date de fin du prêt",
    );
    await user.selectOptions(bookSelection, bookOptions[1]);
    await user.selectOptions(friendSelection, friendOptions[1]);
    await user.clear(loanStartDateSelection);
    await user.type(loanStartDateSelection, today_iso8601);
    await user.clear(loanEndDateSelection);
    await user.type(loanEndDateSelection, one_week_later_iso8601);
    await user.click(loanCreationButton);
    const errorMessage = screen.getByText(/404/i);
    expect(errorMessage).toBeVisible();
  });
});
