import { cleanup, screen, within } from "@testing-library/vue";
import { afterEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../pages/friends/[user].vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import friendshipsFakeData from "./fixtures/friendships.json";
import ownedBooksFakeData from "./fixtures/books/multipleownedbooks.json";
expect.extend(matchers);

const user_id = "1234567890";
const friendship = friendshipsFakeData[0];
const friendshipRequested = friendshipsFakeData[1];

const availableBooks = ownedBooksFakeData.filter(
  (book) => book.status === "available",
);
const loanedBooks = ownedBooksFakeData.filter(
  (book) => book.status === "loaned",
);
const books = [...availableBooks, ...loanedBooks];

describe("friend/[user] page dynamic test suite", () => {
  const mocks = vi.hoisted(() => {
    return {
      fetchFriendship: vi.fn(),
      fetchOwnedBooks: vi.fn(),
    };
  });

  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: {
            username: "Camilledu91",
            user_id: "1234567890",
            token: "9876543210",
          },
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  const { useRouteMock } = vi.hoisted(() => {
    return {
      useRouteMock: vi.fn().mockImplementation(() => {
        return {
          params: {
            user: friendship.friendship_id,
          },
        };
      }),
    };
  });
  mockNuxtImport("useRoute", () => {
    return useRouteMock;
  });

  registerEndpoint(
    `/users/${user_id}/friendships/${friendship.friendship_id}/`,
    mocks.fetchFriendship,
  );

  registerEndpoint(
    `/users/${friendship.friend.user_id}/owned-books/`,
    mocks.fetchOwnedBooks,
  );

  afterEach(() => cleanup());

  it("displays a title with username if user is a friend with friendship status FRIENDSHIP_ACTIVE", async () => {
    mocks.fetchFriendship.mockReturnValue(friendship);
    await renderSuspended(Component);
    const title = screen.getByText(/Bibliothèque de/i);
    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent(
      `Bibliothèque de ${friendship.friend.username}`,
    );
    const errorMessage = screen.queryByText(
      /Vous n'êtes pas autorisé à accéder à ce profil./i,
    );
    expect(errorMessage).not.toBeInTheDocument();
  });

  it("displays an error message if friendship is different than FRIENDSHIP_ACTIVE", async () => {
    mocks.fetchFriendship.mockReturnValue(friendshipRequested);
    await renderSuspended(Component);
    const title = screen.queryByText(/Bibliothèque de/i);
    expect(title).not.toBeInTheDocument();
    const errorMessage = screen.getByText(
      /Vous n'êtes pas autorisé à accéder à ce profil./i,
    );
    expect(errorMessage).toBeInTheDocument();
  });

  it("displays an error message if user is not a friend or doesn't exist", async () => {
    mocks.fetchFriendship.mockResolvedValue(
      new Response(null, { status: 404 }),
    );
    await renderSuspended(Component);
    const title = screen.queryByText(/Bibliothèque de/i);
    expect(title).not.toBeInTheDocument();
    const errorMessage = screen.getByText(
      /Vous n'êtes pas autorisé à accéder à ce profil./i,
    );
    expect(errorMessage).toBeInTheDocument();
  });
  it("displays an error message if fetchFriendship doesn't work", async () => {
    mocks.fetchFriendship.mockReturnValue(Promise.reject({ data: "error" }));
    await renderSuspended(Component);
    const title = screen.queryByText(/Bibliothèque de/i);
    expect(title).not.toBeInTheDocument();
    const errorMessage = screen.getByText(
      /Une erreur est survenue, veuillez réessayez plus tard./i,
    );
    expect(errorMessage).toBeInTheDocument();
  });

  it("displays user's available and loaned books when sucessfully fetched", async () => {
    mocks.fetchFriendship.mockReturnValue(friendship);
    mocks.fetchOwnedBooks.mockReturnValue(books);
    await renderSuspended(Component);
    const bookList = screen.getByRole("list");
    const bookCards = within(bookList).getAllByRole("listitem");
    expect(bookList).toBeInTheDocument();
    expect(bookCards.length).toEqual(books.length);
    books.forEach((book) => {
      const title = screen.getByText(book.book.title);
      expect(title).toBeInTheDocument();
      const author = screen.getByText(
        book.book.author_fname + " " + book.book.author_lname,
      );
      expect(author).toBeInTheDocument();
    });
  });

  it("displays en error message if books fetch fails", async () => {
    mocks.fetchFriendship.mockReturnValue(friendship);
    mocks.fetchOwnedBooks.mockRejectedValue(new Error("error"));
    await renderSuspended(Component);
    const bookList = screen.queryByRole("list");
    const bookCard = screen.queryByRole("listitem");
    expect(bookList).not.toBeInTheDocument();
    expect(bookCard).not.toBeInTheDocument();
    const errorMessage = screen.getByText(/Aucun livre n'a été trouvé./i);
    expect(errorMessage).toBeInTheDocument();
  });
});
