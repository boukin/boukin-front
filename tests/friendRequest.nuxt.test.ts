import { cleanup, fireEvent, screen, waitFor } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/molecules/friend-request.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import type { Friendship } from "../types/friendship.ts";
import type { Props as FriendRequestProps } from "../components/molecules/friend-request.vue";
expect.extend(matchers);

const friendships: Friendship[] = require("./fixtures/friendships.json");
const friendshipRequest = friendships.filter(
  (friendship: Friendship) =>
    friendship.status === "friendship_requested" &&
    friendship.friend.is_requesting_user,
)[0];

describe("friends request card static test suite", () => {
  const props: FriendRequestProps = {
    friendshipId: friendshipRequest.friendship_id,
    userId: friendshipRequest.friend.user_id,
    username: friendshipRequest.friend.username,
  };

  beforeEach(async () => {
    await renderSuspended(Component, { props });
  });
  afterEach(() => cleanup());

  it("displays a text with requesting user's username ", () => {
    expect(
      screen.getByText(`${props.username} souhaite devenir votre ami·e.`),
    ).toBeInTheDocument();
  });
  it("displays a button labelled 'Accepter' with an icon", () => {
    const icon = screen.getByRole("img", { name: "Accepter l'amitié" });
    const text = screen.getByText("Accepter");
    expect(text).toBeInTheDocument();
    expect(icon).toBeInTheDocument();
  });
  it("displays a button labelled 'Refuser' with an icon", () => {
    const icon = screen.getByRole("img", { name: "Refuser l'amitié" });
    const text = screen.getByText("Refuser");
    expect(text).toBeInTheDocument();
    expect(icon).toBeInTheDocument();
  });
});

describe("friends request card dynamic test suite", () => {
  const props: FriendRequestProps = {
    friendshipId: friendshipRequest.friendship_id,
    userId: friendshipRequest.friend.user_id,
    username: friendshipRequest.friend.username,
  };

  beforeEach(async () => {
    await renderSuspended(Component, { props });
  });
  afterEach(() => cleanup());

  const { updateFriendship } = vi.hoisted(() => {
    return {
      updateFriendship: vi.fn(),
    };
  });

  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: {
            email: "Camilledu91@wanadoo.fr",
            user_id: "1234567890",
            token: "9876543210",
            username: "camille",
          },
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  const apiResponse = new Response(null, { status: 204 });
  updateFriendship.mockResolvedValue(apiResponse);
  registerEndpoint(`/friendships/${props.friendshipId}/`, updateFriendship);

  it("displays a 'friendship accepted' message with user's profile link after pressing the accept friendship button", async () => {
    const acceptButton = screen.getByRole("button", {
      name: "Accepter l'amitié",
    });
    // Vérifie que le bouton accepter est présent
    expect(acceptButton).toBeInTheDocument();
    await fireEvent.click(acceptButton);
    await waitFor(() => {
      const messageElement = screen.getByText(/Vous êtes désormais ami·e avec/);
      //Vérifie que le message a bien changé
      expect(messageElement).toBeInTheDocument();
      const link = screen.getByRole("link", {
        name: props.username,
      });
      expect(link).toHaveAttribute("href", `/friends/${props.friendshipId}`);
      // Vérifie que le message contient bien un lien vers le profil de l'utilisateur
      expect(messageElement).toContain(link);
    });
  });

  it("displays a 'friendship denied' message after pressing the deny friendship button", async () => {
    const denyButton = screen.getByRole("button", {
      name: "Refuser l'amitié",
    });
    // Vérifie que le bouton refuser est présent
    expect(denyButton).toBeInTheDocument();
    await fireEvent.click(denyButton);
    await waitFor(() => {
      const messageElement = screen.getByText(/Demande d'amitié refusée./);
      //Vérifie que le message a bien changé
      expect(messageElement).toBeInTheDocument();
    });
  });

  it("hides both buttons and 'wants to be your friend' text when clicking on accept request", async () => {
    const acceptButton = screen.getByRole("button", {
      name: "Accepter l'amitié",
    });
    const denyButton = screen.getByRole("button", {
      name: "Refuser l'amitié",
    });
    expect(acceptButton).toBeInTheDocument();
    expect(denyButton).toBeInTheDocument();
    expect(
      screen.getByText(/souhaite devenir votre ami·e./i),
    ).toBeInTheDocument();
    await fireEvent.click(denyButton);
    await waitFor(() => {
      expect(acceptButton).not.toBeInTheDocument();
      expect(denyButton).not.toBeInTheDocument();
      expect(
        screen.queryByText(/souhaite devenir votre ami·e./i),
      ).not.toBeInTheDocument();
    });
  });

  it("hides both buttons and 'wants to be your friend' text when clicking on deny request", async () => {
    const acceptButton = screen.getByRole("button", {
      name: "Accepter l'amitié",
    });
    const denyButton = screen.getByRole("button", {
      name: "Refuser l'amitié",
    });
    expect(acceptButton).toBeInTheDocument();
    expect(denyButton).toBeInTheDocument();
    expect(
      screen.getByText(/souhaite devenir votre ami·e./i),
    ).toBeInTheDocument();
    await fireEvent.click(denyButton);
    await waitFor(() => {
      expect(acceptButton).not.toBeInTheDocument();
      expect(denyButton).not.toBeInTheDocument();
      expect(
        screen.queryByText(/souhaite devenir votre ami·e./i),
      ).not.toBeInTheDocument();
    });
  });
});
