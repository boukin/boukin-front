import { cleanup, render, screen } from "@testing-library/vue";
import {
  afterAll,
  afterEach,
  beforeAll,
  beforeEach,
  describe,
  expect,
  it,
  vi,
} from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "~/components/molecules/book-add.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import type { Book } from "~/types/boukinApi";
import type { Props as BookAddProps } from "~/components/molecules/book-add.vue";
import userEvent from "@testing-library/user-event";
expect.extend(matchers);

const books: Book[] = require("./fixtures/booksSearch/booksearchbytitle.json");
const book = books[0];
const props: BookAddProps = {
  bookId: book.book_id,
  bookTitle: book.title,
  authorFirstName: book.author_fname,
  authorLastName: book.author_lname,
};

const { addBook } = vi.hoisted(() => {
  return { addBook: vi.fn() };
});
const authenticatedUserId = "3fa85f64-5717-4562-b3fc-2c963f66afa7";
const { useCookieMock } = vi.hoisted(() => {
  return {
    useCookieMock: vi.fn().mockImplementation(() => {
      return {
        value: {
          username: "JujuDu17",
          user_id: authenticatedUserId,
          token: "9876543210",
        },
      };
    }),
  };
});
mockNuxtImport("useCookie", () => {
  return useCookieMock;
});

describe("book add card static test suite", () => {
  beforeAll(() => {
    render(Component, { props });
  });
  afterAll(() => cleanup());

  it("displays book's title and author", () => {
    expect(
      screen.getByText(props.bookTitle, { exact: false }),
    ).toBeInTheDocument();
    expect(
      screen.getByText(props.authorFirstName, { exact: false }),
    ).toBeInTheDocument();
    expect(
      screen.getByText(props.authorLastName, { exact: false }),
    ).toBeInTheDocument();
  });
  it("displays a 'Ajouter' button", () => {
    expect(
      screen.getByRole("button", { name: /Ajouter/i }),
    ).toBeInTheDocument();
  });
});

describe("book add card dynamic test suite", () => {
  beforeEach(async () => {
    await renderSuspended(Component, { props });
  });
  afterEach(() => cleanup());

  it("displays a confirmation message when user presses 'Ajouter' button and request is successful", async () => {
    const user = userEvent.setup();
    const addButton = screen.getByRole("button", { name: /Ajouter/i });
    const apiSuccessResponse = {
      data: {
        value: {
          owned_book_id: "e174c340-0db7-4b93-ab45-a2a50412182d",
          owner: authenticatedUserId,
          book: props.bookId,
          status: "available",
        },
      },
      error: { value: null },
    };
    addBook.mockReturnValue(apiSuccessResponse);
    registerEndpoint(`/owned-books/`, addBook);
    const confirmationMessage = "Livre ajouté à la bibliothèque";

    await user.click(addButton);

    expect(screen.getByText(confirmationMessage)).toBeInTheDocument();
  });

  it("displays an error message if adding request fails", async () => {
    const user = userEvent.setup();
    const addButton = screen.getByRole("button", { name: /Ajouter/i });
    const apiErrorResponse = {
      message: "Error",
    };
    addBook.mockRejectedValue(apiErrorResponse);
    registerEndpoint(`/owned-books/`, addBook);
    const errorMessage = /Une erreur s'est produite lors de l'ajout/i;

    await user.click(addButton);

    expect(screen.getByText(errorMessage)).toBeInTheDocument();
  });
});
