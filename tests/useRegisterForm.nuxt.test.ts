import { describe, expect, it } from "vitest";
import { useRegisterForm } from "../composables/useRegisterForm";

const { validatePassword, validateRegisterForm } = useRegisterForm();

describe("validatePassword", () => {
  const validPassword = "validP@ssw0rd";
  const differentPassword = "differentP@ssw0rd";
  const passwordWithoutSymbol = "invalidPassw0rd";
  const passwordWithoutDigit = "invalidP@ssword";
  const passwordWithoutUppercase = "invalidp@ssw0rd";
  const passwordTooLong = "ThisP@ssw0rdIsWayTooLong!";

  it("returns true if valid password and matching confirmation", () => {
    expect(validatePassword(validPassword, validPassword)).toBe(true);
  });

  it("returns false if non-matching passwords", () => {
    expect(validatePassword(validPassword, differentPassword)).toBe(false);
  });

  it("returns false if password does not pass the regex", () => {
    expect(validatePassword(passwordWithoutSymbol, passwordWithoutSymbol)).toBe(
      false,
    );
    expect(validatePassword(passwordTooLong, passwordTooLong)).toBe(false);
    expect(
      validatePassword(passwordWithoutUppercase, passwordWithoutUppercase),
    ).toBe(false);
    expect(validatePassword(passwordWithoutDigit, passwordWithoutDigit)).toBe(
      false,
    );
  });
});

describe("validateRegisterForm", () => {
  it("returns true if every fields are filled", () => {
    const form = {
      username: "username",
      email: "email",
      password: "password",
      passwordConfirmation: "passwordConfirmation",
    };
    expect(validateRegisterForm(form)).toBe(true);
  });
  it("returns false if one field is empty", () => {
    const form = {
      username: "username",
      email: "email",
      password: "password",
      passwordConfirmation: "",
    };
    expect(validateRegisterForm(form)).toBe(false);
  });
  it("returns false if type of form is not UserRegistration", () => {
    const form = {
      username: "username",
      email: "email",
      password: "password",
    };
    expect(validateRegisterForm(form)).toBe(false);
  });
});
