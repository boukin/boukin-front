import { cleanup, render, screen } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/organisms/friends-request-section.vue";

expect.extend(matchers);

describe("friends request section test suite", () => {
  beforeEach(() => {
    render(Component, {
      props: {
        friendshipRequests: [
          {
            friendship_id: "123",
            status: "friendship_requested",
            friend: {
              is_requesting_user: true,
              email: "cam@mail.com",
              role: "user",
              username: "Camilledu91",
              user_id: "72e5e768-f9a3-414f-a1aa-8d679f91d8c4",
            },
          },
        ],
      },
    });
  });
  afterEach(() => cleanup());

  it("displays a 'Mes invitations' heading", () => {
    const friendsRequestSectionHeading = screen.getByRole("heading", {
      name: "Mes invitations en attente :",
    });
    expect(friendsRequestSectionHeading).toBeInTheDocument();
  });

  it("displays a 'No request' text if no friend request ", () => {
    cleanup();
    render(Component, {
      props: {
        friendshipRequests: [],
      },
    });
    const noRequestText = screen.getByText(
      "Vous n'avez pas d'invitation pour le moment.",
    );
    expect(noRequestText).toBeInTheDocument();
  });
  it("displays requesting user's username when there is an invitation ", () => {
    const requestingUsername = screen.getByText(/Camilledu91/i);
    expect(requestingUsername).toBeInTheDocument();
  });
  it("displays a button labelled 'Accepter' when there is an invitation", () => {
    const acceptButton = screen.getByRole("button", { name: /Accepter/i });
    expect(acceptButton).toBeInTheDocument();
  });
  it("displays a button labelled 'Refuser' when there is an invitation", () => {
    const denyButton = screen.getByRole("button", { name: /Refuser/i });
    expect(denyButton).toBeInTheDocument();
  });
});
