import { cleanup, render, screen } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/organisms/friends-list-section.vue";

expect.extend(matchers);

describe("friends list section test suite", () => {
  beforeEach(() => {
    render(Component);
  });
  afterEach(() => cleanup());

  it("displays a 'Ma liste d'ami·e·s' heading", () => {
    const friendsRequestSectionHeading = screen.getByRole("heading", {
      name: "Ma liste d'ami·e·s :",
    });
    expect(friendsRequestSectionHeading).toBeInTheDocument();
  });

  it("displays a 'No friendship' text when user has no friends", () => {
    cleanup();
    render(Component, {
      props: {
        friendships: [],
      },
    });
    const noFriendshipText = screen.getByText(
      "Vous n'avez pas d'amitiés pour le moment.",
    );
    expect(noFriendshipText).toBeInTheDocument();
  });
  it("displays friend's username", () => {
    cleanup();
    render(Component, {
      props: {
        friendships: [
          {
            friend: {
              username: "Camilledu91",
              user_id: "72e5e768-f9a3-414f-a1aa-8d679f91d8c4",
              email: "camille@test.com",
              role: "user",
              is_requesting_user: false,
            },
            friendship_id: "1111111561561561",
            status: "friendship_active",
          },
        ],
      },
    });
    const requestingUsername = screen.getByText(/Camilledu91/i);
    expect(requestingUsername).toBeInTheDocument();
  });
});
