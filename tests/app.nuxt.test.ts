import { describe, it, expect } from "vitest";
import { setup, $fetch, createPage, url } from "@nuxt/test-utils/e2e";
import { userEvent } from "@testing-library/user-event";
const path = require("path");

describe("register page test suite", async () => {
  await setup({
    rootDir: path.resolve(__dirname, "../"),
    browser: true,
    host: "http://localhost:3001",
    port: 3001,
  });

  it.skip("registers the user after filling the form inputs with valid informations", async () => {
    const registerPage = await createPage("/register");
    expect(
      await registerPage.getByRole("heading", { name: "Boukin" }).isVisible(),
    ).toBe(true);
    expect(await registerPage.getByText("inscription").isVisible()).toBe(true);
    await registerPage.getByText("inscription").click();
    await registerPage
      .getByRole("textbox", { name: "email" })
      .fill("testboukin@boukin.org");
    await registerPage
      .getByRole("textbox", { name: "username" })
      .fill("testboukin");
    await registerPage
      .getByRole("textbox", { name: "password", exact: true })
      .fill("PassW0rd@");
    await registerPage
      .getByRole("textbox", { name: "passwordConfirmation", exact: true })
      .fill("PassW0rd@");
    await registerPage
      .getByRole("button", { name: "Créer mon compte" })
      .click();
  });
});
