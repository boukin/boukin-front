import { describe, expect, it } from "vitest";
import { useForm } from "../composables/useForm";

const { cleanForm } = useForm();

describe("cleanForm test suite", () => {
  const formDataWithoutEmptyFields = {
    email: "email@test.com",
    username: "userTest",
    passWord: "P@ssw0rd!",
  };
  const formDataWithEmptyFields = {
    email: "",
    username: "userTest",
    passWord: "",
  };
  const formDataWithOnlyEmptyFields = {
    email: "",
    username: "",
    passWord: "",
  };
  const formDataWithSpaces = {
    email: "email@test.com",
    username: "     ",
    passWord: "",
  };

  it("returns the same object when form fields are all filled", () => {
    expect(cleanForm(formDataWithoutEmptyFields)).toEqual(
      formDataWithoutEmptyFields,
    );
  });

  it("trims form's empty fields when there are some", () => {
    expect(cleanForm(formDataWithEmptyFields)).toEqual({
      username: "userTest",
    });
  });

  it("returns empty object if no fields are filled ", () => {
    expect(cleanForm(formDataWithOnlyEmptyFields)).toEqual({});
  });

  it("trims fields if they contains only spaces ", () => {
    expect(cleanForm(formDataWithSpaces)).toEqual({
      email: "email@test.com",
    });
  });
});
