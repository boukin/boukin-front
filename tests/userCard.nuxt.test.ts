import {
  cleanup,
  render,
  screen,
  fireEvent,
  waitFor,
} from "@testing-library/vue";
import { afterEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/molecules/user-card.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import type { Props as UserCardProps } from "../components/molecules/user-card.vue";

expect.extend(matchers);

describe("friends card section static test suite", () => {
  afterEach(() => cleanup());

  it("displays username and email", () => {
    render(Component, {
      props: {
        username: "Camille91",
        email: "camille@test.com",
        variant: "active",
        userId: "123",
      },
    });
    const username = screen.getByText(/Camille91/i);
    const email = screen.getByText(/camille@test.com/i);
    expect(username).toBeInTheDocument();
    expect(email).toBeInTheDocument();
  });
  it("displays a request button when variant is active", () => {
    render(Component, {
      props: {
        username: "Camille",
        email: "camille@test.com",
        variant: "active",
        userId: "123",
      },
    });
    const requestButton = screen.getByRole("button", {
      name: "Envoyer une demande",
    });
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).not.toBeDisabled();
  });
  it("displays a disabled request button when variant is disabled", () => {
    render(Component, {
      props: {
        username: "Camille",
        email: "camille@test.com",
        variant: "disabled",
        userId: "123",
      },
    });
    const requestButton = screen.getByRole("button", {
      name: "Envoyer une demande",
    });
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).toBeDisabled();
  });
  it("displays a already friends text and no button when variant is alreadyFriends", () => {
    render(Component, {
      props: {
        username: "Camille",
        email: "camille@test.com",
        variant: "alreadyFriends",
        userId: "123",
      },
    });
    const requestButton = screen.queryByRole("button", {
      name: "Envoyer une demande",
    });
    const alreadyFriendsMessage = screen.getByText(/Vous êtes déjà ami·e·s./i);
    expect(requestButton).not.toBeInTheDocument();
    expect(alreadyFriendsMessage).toBeInTheDocument();
  });
  it("displays a request sent text and no button when variant is requestSent", () => {
    render(Component, {
      props: {
        username: "Camille",
        email: "camille@test.com",
        variant: "requestSent",
        userId: "123456789",
      },
    });
    const requestButton = screen.queryByRole("button", {
      name: "Envoyer une demande",
    });
    const requestSentMessage = screen.getByText(/Demande envoyée !/i);
    expect(requestButton).not.toBeInTheDocument();
    expect(requestSentMessage).toBeInTheDocument();
  });
});

describe("user card section dynamic test suite", () => {
  const users = require("./fixtures/users-search.json");

  const props: UserCardProps = {
    username: users.not_friend.username,
    email: users.not_friend.email,
    variant: "active",
    userId: users.not_friend.user_id,
    friendshipId: undefined,
  };

  const { sendFriendshipRequest } = vi.hoisted(() => {
    return {
      sendFriendshipRequest: vi.fn(),
    };
  });
  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: {
            email: "Camilledu91@wanadoo.fr",
            user_id: "1234567890",
            token: "9876543210",
            username: "camille",
          },
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  afterEach(() => cleanup());

  it("displays a 'Friend request sent' message when request is successfully sent", async () => {
    await renderSuspended(Component, { props });
    const apiSuccessResponse = {
      data: {
        value: [
          {
            requesting_user: "123456789",
            addressed_user: users.not_friend.user_id,
          },
        ],
      },
      error: { value: null },
    };

    sendFriendshipRequest.mockResolvedValue(apiSuccessResponse);
    registerEndpoint("/friendships/", sendFriendshipRequest);

    const requestButton = screen.getByRole("button", {
      name: "Envoyer une demande",
    });
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).not.toBeDisabled();
    await fireEvent.click(requestButton);
    await waitFor(() => {
      const confirmationMessage = screen.getByText(/Demande envoyée !/i);
      expect(confirmationMessage).toBeInTheDocument();
      expect(
        screen.queryByRole("button", {
          name: "Envoyer une demande",
        }),
      ).not.toBeInTheDocument();
    });
  });

  it("display error message if request sending fails", async () => {
    await renderSuspended(Component, { props });

    const apiErrorResponse = {
      message: "Error message",
    };

    sendFriendshipRequest.mockRejectedValue(apiErrorResponse);
    registerEndpoint("/friendships/", sendFriendshipRequest);

    const requestButton = screen.getByRole("button", {
      name: "Envoyer une demande",
    });
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).not.toBeDisabled();
    await fireEvent.click(requestButton);
    await waitFor(() => {
      const errorMessage = screen.getByText(/Demande d'amitié impossible/i);
      expect(errorMessage).toBeInTheDocument();
      expect(
        screen.queryByRole("button", {
          name: "Envoyer une demande",
        }),
      ).not.toBeInTheDocument();
    });
  });
});
