import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import {
  cleanup,
  render,
  fireEvent,
  screen,
  waitFor,
} from "@testing-library/vue";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "@/components/molecules/ownedbook-card.vue";
import type { Props as OwnedBookCardProps } from "~/components/molecules/ownedbook-card.vue";
import ownedBooksFakeData from "./fixtures/books/multipleownedbooks.json";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";

expect.extend(matchers);

const availableOwnedBooks = ownedBooksFakeData.filter(
  (book) => book.status === "available",
);
const loanedOwnedBooks = ownedBooksFakeData.filter(
  (book) => book.status === "loaned",
);

describe("ownedbooks card display test suite", () => {
  let props: OwnedBookCardProps;
  beforeEach(() => {
    props = {
      title: "Ce livre est dispo",
      author: "Jane Doe",
      status: "available",
    };
    render(Component, { props });
  });
  afterEach(() => cleanup());

  it("displays book title", () => {
    const bookTitle = screen.getByText(props.title);
    expect(bookTitle).toBeInTheDocument();
  });
  it("displays book author name", () => {
    const author = screen.getByText(props.author);
    expect(author).toBeInTheDocument();
  });
  it("displays a default image if none provided", () => {
    const alt_description_image = "Image par défaut du livre";
    expect(
      screen.getByRole("img", { name: alt_description_image }),
    ).toBeInTheDocument();
  });
});

describe("ownedbook card with request button test suite", () => {
  const availableBook: OwnedBookCardProps = {
    title: availableOwnedBooks[0].book.title,
    author:
      availableOwnedBooks[0].book.author_fname +
      " " +
      availableOwnedBooks[0].book.author_lname,
    status: availableOwnedBooks[0].status as OwnedBookCardProps["status"],
    hasLoanButton: true,
    ownedBookId: availableOwnedBooks[0].owned_book_id,
    ownerId: "book-owner-uuid",
  };

  const alreadyLoanedBook: OwnedBookCardProps = {
    title: loanedOwnedBooks[0].book.title,
    author:
      loanedOwnedBooks[0].book.author_fname +
      " " +
      loanedOwnedBooks[0].book.author_lname,
    status: loanedOwnedBooks[0].status as OwnedBookCardProps["status"],
    hasLoanButton: true,
    ownedBookId: loanedOwnedBooks[0].owned_book_id,
    ownerId: "book-owner-uuid",
  };

  const { sendLoanRequest } = vi.hoisted(() => {
    return {
      sendLoanRequest: vi.fn(),
    };
  });

  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: {
            username: "Camilledu91",
            user_id: "1234567890",
            token: "9876543210",
          },
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  afterEach(() => cleanup());

  it("displays a loan request button when book is available", async () => {
    await renderSuspended(Component, { props: availableBook });
    const requestButton = screen.getByRole("button", {
      name: "Emprunter",
    });
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).not.toBeDisabled();
  });

  it("displays a disabled button when book is already loaned", async () => {
    await renderSuspended(Component, { props: alreadyLoanedBook });
    const unavailableButton = screen.getByRole("button", {
      name: "Indisponible",
    });
    expect(unavailableButton).toBeInTheDocument();
    expect(unavailableButton).toBeDisabled();
  });

  it("displays a success message instead of the button when request is successful", async () => {
    await renderSuspended(Component, { props: availableBook });
    const apiResponse = new Response(null, { status: 201 });
    sendLoanRequest.mockReturnValue(apiResponse);
    registerEndpoint("/loans/request/", sendLoanRequest);
    const requestButton = screen.getByRole("button", {
      name: "Emprunter",
    });
    await fireEvent.click(requestButton);
    await waitFor(() => {
      const confirmationMessage = screen.getByText("Demande envoyée !");
      expect(confirmationMessage).toBeInTheDocument();
      expect(
        screen.queryByRole("button", {
          name: "Emprunter",
        }),
      ).not.toBeInTheDocument();
    });
  });

  it("displays an error message instead of the button when request fails", async () => {
    await renderSuspended(Component, { props: availableBook });
    const apiErrorResponse = new Response(null, { status: 404 });
    sendLoanRequest.mockReturnValue(apiErrorResponse);
    registerEndpoint("/loans/request/", sendLoanRequest);
    const requestButton = screen.getByRole("button", {
      name: "Emprunter",
    });
    await fireEvent.click(requestButton);
    await waitFor(() => {
      const confirmationMessage = screen.getByText("Demande impossible");
      expect(confirmationMessage).toBeInTheDocument();
      expect(
        screen.queryByRole("button", {
          name: "Emprunter",
        }),
      ).not.toBeInTheDocument();
    });
  });
});
