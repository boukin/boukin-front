import { cleanup, render, screen, fireEvent } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/atoms/button.vue";

expect.extend(matchers);

describe("AtomsButton test suite", () => {
  beforeEach(() => {
    render(Component, {
      props: {
        label: "Ok",
      },
    });
  });
  afterEach(() => cleanup());

  it("displays a button with 'Ok' as a label ", () => {
    const okButton = screen.getByRole("button", {
      name: "Ok",
    });
    expect(okButton).toBeInTheDocument();
  });

  it("emits a click event when clicked", async () => {
    cleanup();
    const screen = render(Component, {
      props: {
        label: "Ok",
      },
    });

    const okButton = screen.getByRole("button", {
      name: "Ok",
    });
    await fireEvent.click(okButton);
    await fireEvent.click(okButton);

    expect(screen.emitted()).toHaveProperty("buttonClick");
    expect(screen.emitted().buttonClick.length).toBe(2);
  });
});
