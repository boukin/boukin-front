import {
  afterAll,
  afterEach,
  beforeAll,
  beforeEach,
  describe,
  expect,
  it,
} from "vitest";
import { cleanup, render, screen } from "@testing-library/vue";
import * as matchers from "@testing-library/jest-dom/matchers";
import userEvent from "@testing-library/user-event";

import newBookPage from "@/pages/books/new.vue";

expect.extend(matchers);

describe("new book page static elements test suite", () => {
  beforeAll(() => {
    render(newBookPage);
  });
  afterAll(() => cleanup());

  it("displays a heading and a link showing the page hierarchy concerning the adding a book website section", () => {
    const myBooksLink = screen.getByRole("link", { name: "Ma bibliothèque" });
    const newBookPageHeading = screen.getByRole("heading", {
      name: /Ajouter un livre/i,
    });
    expect(myBooksLink).toBeVisible();
    expect(newBookPageHeading).toBeVisible();
  });
  it("displays book search section", () => {
    const searchSection = screen.getByRole("region", {
      name: "Barre de recherche de livres",
    });
    const isbnSearchBar = screen.getByRole("textbox", {
      name: "isbn",
    });
    expect(searchSection).toBeInTheDocument();
    expect(isbnSearchBar).toBeInTheDocument();
  });
  it("displays a switch element with options to choose between search an already existing book and add one manually", () => {
    const searchFilter = screen.getByRole("radio", {
      name: /recherche/i,
    });
    const manualAddingFilter = screen.getByRole("radio", {
      name: /ajout/i,
    });
    expect(searchFilter).toBeInTheDocument();
    expect(manualAddingFilter).toBeInTheDocument();
  });
});

describe("new book page dynamic elements test suite", () => {
  beforeEach(() => {
    render(newBookPage);
  });
  afterEach(() => cleanup());

  it("displays new book addition form when user clicks on switch button 'ajout manuel' option and hides search form", async () => {
    const user = userEvent.setup();
    const manualAddingFilter = screen.getByRole("radio", {
      name: /ajout/i,
    });
    const searchSection = screen.queryByRole("region", {
      name: "Barre de recherche de livres",
    });
    const newBookForm = screen.queryByRole("form", {
      name: "Ajout d'un livre",
    });
    expect(searchSection).toBeInTheDocument();
    expect(newBookForm).not.toBeInTheDocument();

    await user.click(manualAddingFilter);

    const searchSectionAfter = screen.queryByRole("region", {
      name: "Barre de recherche de livres",
    });
    const newBookFormAfter = screen.queryByRole("form", {
      name: "Ajout d'un livre",
    });
    expect(searchSectionAfter).not.toBeInTheDocument();
    expect(newBookFormAfter).toBeInTheDocument();
  });
});
