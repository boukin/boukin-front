import { cleanup, screen, fireEvent, waitFor } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/molecules/confirmation-modal.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import type { Friendship } from "~/types/friendship";

expect.extend(matchers);

describe("Confirmation modal static test suite", () => {
  let deleteButton: HTMLElement, cancelButton: HTMLElement;

  beforeEach(async () => {
    await renderSuspended(Component, {
      props: {
        friendshipId: "123",
        username: "Camilledu91",
      },
    });

    deleteButton = screen.getByRole("button", {
      name: /Oui, supprimer/i,
    });
    cancelButton = screen.getByRole("button", { name: /Annuler/i });
  });
  afterEach(() => cleanup());

  it("displays a text with username and two buttons", async () => {
    const deleteQuestion = screen.getByText(
      /Êtes-vous sûr·e de vouloir supprimer votre amitié/i,
    );
    expect(deleteQuestion).toBeInTheDocument();
    expect(cancelButton).toBeInTheDocument();
    expect(deleteButton).toBeInTheDocument();
    expect(deleteQuestion).toHaveTextContent(/Camilledu91/i);
  });
});

describe("Confirmation modal dynamic test suite", () => {
  const friendships: Friendship[] = require("./fixtures/friendships.json");
  const props = {
    friendshipId: friendships[0].friendship_id,
    username: friendships[0].friend.username,
  };

  const { updateFriendship } = vi.hoisted(() => {
    return {
      updateFriendship: vi.fn(),
    };
  });

  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: {
            email: "Camilledu91@wanadoo.fr",
            user_id: "1234567890",
            token: "9876543210",
            username: "camille",
          },
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  afterEach(() => cleanup());

  it("displays a friendship over message after pressing the delete friendship button and successful request", async () => {
    await renderSuspended(Component, { props });
    updateFriendship.mockReturnValue(new Response(null, { status: 204 }));

    registerEndpoint(`/friendships/${props.friendshipId}/`, updateFriendship);

    const deleteButton = screen.getByRole("button", {
      name: "Oui, supprimer",
    });
    expect(deleteButton).toBeInTheDocument();
    expect(screen.queryByText(/Amitié supprimée/i)).not.toBeInTheDocument();
    await fireEvent.click(deleteButton);
    await waitFor(() => {
      const messageElement = screen.getByText(/Amitié supprimée./);
      expect(messageElement).toBeInTheDocument();
      expect(
        screen.queryByText(
          /Êtes-vous sûr·e de vouloir supprimer votre amitié/i,
        ),
      ).not.toBeInTheDocument();
    });
    expect(
      screen.queryByRole("button", { name: /Oui, supprimer/i }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("button", { name: /Annuler/i }),
    ).not.toBeInTheDocument();
  });

  it("displays an error message after pressing the delete friendship button and failed request", async () => {
    await renderSuspended(Component, { props });
    const apiErrorResponse = {
      value: { message: "Error message" },
    };
    updateFriendship.mockRejectedValue(apiErrorResponse);
    registerEndpoint(`/friendships/${props.friendshipId}/`, updateFriendship);

    const deleteButton = screen.getByRole("button", {
      name: "Oui, supprimer",
    });
    expect(deleteButton).toBeInTheDocument();
    await fireEvent.click(deleteButton);
    await waitFor(() => {
      const message = screen.getByText(/Suppression impossible/);
      expect(message).toBeInTheDocument();
      expect(
        screen.queryByText(
          /Êtes-vous sûr·e de vouloir supprimer votre amitié/i,
        ),
      ).not.toBeInTheDocument();
    });
    expect(
      screen.queryByRole("button", { name: /Oui, supprimer/i }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("button", { name: /Annuler/i }),
    ).not.toBeInTheDocument();
  });
});
