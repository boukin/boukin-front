import { cleanup, render, screen } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../components/molecules/friend-card.vue";
import type { Props as FriendCardProps } from "../components/molecules/friend-card.vue";
import userEvent from "@testing-library/user-event";
expect.extend(matchers);

describe("friends card section test suite", () => {
  let props: FriendCardProps;
  beforeEach(() => {
    props = {
      username: "Camilledu91",
      userId: "72e5e768-f9a3-414f-a1aa-8d679f91d8c4",
      email: "camilledu91@mail.com",
      friendshipId: "1115e768-f9a3-414f-a1aa-8d679123d8c4",
    };
    render(Component, {
      props,
    });
  });
  afterEach(() => cleanup());

  it("displays username ", () => {
    const requestingUsername = screen.getByText(props.username);
    expect(requestingUsername).toBeInTheDocument();
  });
  it("displays a link to user's page", () => {
    const linkElement = screen.getByRole("link");
    expect(linkElement).toHaveAttribute(
      "href",
      `/friends/${props.friendshipId}/`,
    );
  });
  it("displays an options button", () => {
    const optionToggle = screen.getByRole("checkbox");
    expect(optionToggle).toBeInTheDocument();
  });
  it("displays friend's e-mail address when clicking on the optionToggle", async () => {
    const user = userEvent.setup();
    const optionToggle = screen.getByRole("checkbox", {
      name: "Afficher les options",
    });
    expect(screen.queryByText(props.email)).not.toBeInTheDocument();
    await user.click(optionToggle);
    expect(screen.getByText(props.email)).toBeInTheDocument();
    await user.click(optionToggle);
    expect(screen.queryByText(props.email)).not.toBeInTheDocument();
  });
  it("displays a delete button when clicking on the optionToggle", async () => {
    const user = userEvent.setup();
    const optionToggle = screen.getByRole("checkbox");
    expect(
      screen.queryByRole("button", { name: "Supprimer l'amitié" }),
    ).not.toBeInTheDocument();
    await user.click(optionToggle);
    expect(
      screen.getByRole("button", { name: "Supprimer l'amitié" }),
    ).toBeInTheDocument();
    await user.click(optionToggle);
    expect(
      screen.queryByRole("button", { name: "Supprimer l'amitié" }),
    ).not.toBeInTheDocument();
  });
  it("displays a confirmation modal when clicking on the delete button", async () => {
    const user = userEvent.setup();
    const optionToggle = screen.getByRole("checkbox");
    await user.click(optionToggle);
    expect(
      screen.getByRole("button", { name: "Supprimer l'amitié" }),
    ).toBeInTheDocument();
    const deleteButton = screen.getByRole("button", {
      name: "Supprimer l'amitié",
    });
    await user.click(deleteButton);
    expect(
      screen.getByText(/Êtes-vous sûr·e de vouloir supprimer votre amitié/i),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /Annuler/i }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /Oui, supprimer/i }),
    ).toBeInTheDocument();
  });
  it("removes the confirmation modal when clicking on cancel button", async () => {
    const user = userEvent.setup();
    const optionToggle = screen.getByRole("checkbox");
    await user.click(optionToggle);
    const deleteButton = screen.getByRole("button", {
      name: "Supprimer l'amitié",
    });
    await user.click(deleteButton);
    expect(
      screen.getByText(/Êtes-vous sûr·e de vouloir supprimer votre amitié/i),
    ).toBeInTheDocument();
    const cancelButton = screen.getByRole("button", {
      name: /Annuler/i,
    });
    await user.click(cancelButton);
    expect(
      screen.queryByText(/Êtes-vous sûr·e de vouloir supprimer votre amitié/i),
    ).not.toBeInTheDocument();
  });
});
