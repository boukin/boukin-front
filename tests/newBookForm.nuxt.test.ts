import {
  afterAll,
  afterEach,
  beforeAll,
  beforeEach,
  describe,
  expect,
  it,
  vi,
} from "vitest";
import { cleanup, render, screen, within } from "@testing-library/vue";
import * as matchers from "@testing-library/jest-dom/matchers";

import Component from "~/components/organisms/new-book-form.vue";
import {
  mockNuxtImport,
  registerEndpoint,
  renderSuspended,
} from "@nuxt/test-utils/runtime";
import userEvent from "@testing-library/user-event";

expect.extend(matchers);

describe("new book form static elements test suite", () => {
  beforeAll(() => {
    render(Component);
  });
  afterAll(() => cleanup());

  it("displays a heading describing the purpose of the form of add a new book", () => {
    const newBookHeading = screen.getByRole("heading", {
      name: "Ajouter un nouveau livre à ma bibliothèque",
    });
    expect(newBookHeading).toBeInTheDocument();
  });
  it("displays a form for adding a new book", () => {
    const newBookForm = screen.getByRole("form", {
      name: "Ajout d'un livre",
    });
    expect(newBookForm).toBeInTheDocument();
  });
  it("displays isbn, title, author's firstname, author's lastname text inputs with placeholders", () => {
    const newBookForm = screen.getByRole("form", {
      name: "Ajout d'un livre",
    });
    const inputParams = [
      { name: "isbn", placeholder: /ISBN du livre/i },
      { name: "title", placeholder: /Titre du livre/i },
      { name: "author_fname", placeholder: /Prénom/i },
      { name: "author_lname", placeholder: /Nom/ },
    ];
    inputParams.forEach((input) => {
      const newBookInput = within(newBookForm).getByRole("textbox", {
        name: input.name,
      });
      expect(newBookInput).toBeInTheDocument();
      expect(newBookInput).toHaveAttribute(
        "placeholder",
        expect.stringMatching(input.placeholder),
      );
    });
  });
  it("displays a button to validate the addition of a new book", () => {
    const newBookForm = screen.getByRole("form", {
      name: "Ajout d'un livre",
    });
    const newBookButton = within(newBookForm).getByRole("button", {
      name: "Ajouter le livre",
    });
    expect(newBookButton).toBeVisible();
  });
});

const userId = "3c48ecd4-d0bd-4171-a034-4444b768fbd8";
const token = "9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b";
const mocks = vi.hoisted(() => {
  return {
    postBook: vi.fn(),
    postOwnedBook: vi.fn(),
  };
});
registerEndpoint(`/books/`, mocks.postBook);
registerEndpoint(`/owned-books/`, mocks.postOwnedBook);
mockNuxtImport("useCookie", () => {
  return () => {
    return {
      value: {
        email: "wowilovebooks@boukin.org",
        user_id: userId,
        token: token,
      },
    };
  };
});

const bookData = require("./fixtures/books/multipleownedbooks.json")[0]["book"];

describe("new book form dynamic elements test suite", () => {
  beforeEach(async () => {
    const apiSuccessResponsePostBook = new Response(bookData, { status: 201 });
    const apiSuccessResponsePostOwnedBook = new Response(null, { status: 201 });
    mocks.postBook.mockResolvedValue(apiSuccessResponsePostBook);
    mocks.postOwnedBook.mockResolvedValue(apiSuccessResponsePostOwnedBook);
    await renderSuspended(Component);
  });
  afterEach(() => {
    vi.clearAllMocks();
    cleanup();
  });

  it("displays a confirmation message after filling all input fields, pressing the add new book button and sending successfully the new book data", async () => {
    const user = userEvent.setup();
    const newBookForm = screen.getByRole("form", {
      name: "Ajout d'un livre",
    });
    const newBookButton = within(newBookForm).getByRole("button", {
      name: "Ajouter le livre",
    });
    const confirmationMessage =
      "Le livre a bien été ajouté à la bibliothèque !";
    expect(screen.queryByText(confirmationMessage)).not.toBeInTheDocument();

    for (const inputName of ["isbn", "title", "author_fname", "author_lname"]) {
      const inputField = within(newBookForm).getByRole("textbox", {
        name: inputName,
      });
      await user.type(inputField, bookData[inputName]);
    }
    await user.click(newBookButton);

    expect(screen.getByText(confirmationMessage)).toBeInTheDocument();
  });

  it("calls the right API urls to create a new book in database if not already created and add it as user's owned book", async () => {
    const user = userEvent.setup();
    const newBookForm = screen.getByRole("form", {
      name: "Ajout d'un livre",
    });
    const newBookButton = within(newBookForm).getByRole("button", {
      name: "Ajouter le livre",
    });

    for (const inputName of ["isbn", "title", "author_fname", "author_lname"]) {
      const inputField = within(newBookForm).getByRole("textbox", {
        name: inputName,
      });
      await user.type(inputField, bookData[inputName]);
    }
    await user.click(newBookButton);

    expect(mocks.postBook).toHaveBeenCalledOnce();
    expect(mocks.postOwnedBook).toHaveBeenCalledOnce();
  });

  it("displays a warning message saying to the user that all fields must be filled to add a new book", async () => {
    const user = userEvent.setup();
    const newBookButton = screen.getByRole("button", {
      name: "Ajouter le livre",
    });
    await user.click(newBookButton);
    const fillFieldsErrorMessage = screen.getByText(
      "Ajout du livre impossible : tous les champs sont obligatoires !",
    );
    expect(fillFieldsErrorMessage).toBeVisible();
  });

  it("displays an error message if the new book addition has failed", async () => {
    const user = userEvent.setup();
    const newBookForm = screen.getByRole("form", {
      name: "Ajout d'un livre",
    });
    const newBookButton = within(newBookForm).getByRole("button", {
      name: "Ajouter le livre",
    });

    for (const inputName of ["isbn", "title", "author_fname", "author_lname"]) {
      const inputField = within(newBookForm).getByRole("textbox", {
        name: inputName,
      });
      await user.type(inputField, bookData[inputName]);
    }

    // error in ownedbook POST request
    const apiErrorResponsePostOwnedBook = new Response(null, { status: 400 });
    mocks.postOwnedBook.mockResolvedValue(apiErrorResponsePostOwnedBook);
    await user.click(newBookButton);
    const ErrorMessageOwnedBook = screen.getByText(
      /Erreur lors de l'ajout du livre/i,
    );
    expect(ErrorMessageOwnedBook).toBeVisible();

    // error in book POST request
    const apiErrorResponsePostBook = new Response(null, { status: 400 });
    mocks.postBook.mockResolvedValue(apiErrorResponsePostBook);
    await user.click(newBookButton);
    const ErrorMessageBook = screen.getByText(
      /Erreur lors de la création du livre/i,
    );
    expect(ErrorMessageBook).toBeVisible();
  });
});
