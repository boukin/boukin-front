import LoanDetails from "../components/organisms/loan-details.vue";
import { ButtonText, LoanUpdateType } from "../types/loan";
import { afterEach, beforeEach, expect, describe, it, vi } from "vitest";
import { cleanup, render, screen, within } from "@testing-library/vue";
import { registerEndpoint, mockNuxtImport } from "@nuxt/test-utils/runtime";
import userEvent from "@testing-library/user-event";
import * as matchers from "@testing-library/jest-dom/matchers";
expect.extend(matchers);

const userId = "3c48ecd4-d0bd-4171-a034-4444b768fbd8";
const token = "9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b";
const loan = require("../tests/fixtures/loans/loan_active.json").loans[0];

const apiSuccessfulResponse = new Response(null, { status: 204 });

const mocks = vi.hoisted(() => {
  return { postUpdateLoanStatus: vi.fn() };
});
registerEndpoint(
  `/users/${userId}/loans/${loan.loan_id}`,
  mocks.postUpdateLoanStatus,
);
mockNuxtImport("useCookie", () => {
  return () => {
    return {
      value: {
        email: "wowilovebooks@boukin.org",
        user_id: userId,
        token: token,
      },
    };
  };
});
mocks.postUpdateLoanStatus.mockReturnValue(apiSuccessfulResponse);

describe("modal for confirming or denying loan request test suite", () => {
  const loanUpdateType = LoanUpdateType.LoanRequest;
  const loanRequestProps = { loan, loanUpdateType };
  beforeEach(() => {
    render(LoanDetails, { props: loanRequestProps });
  });
  afterEach(() => {
    cleanup();
  });

  it("displays a sentence describing that another user wants to request a book owned by the user", async () => {
    const user = userEvent.setup();
    const decisionButton = screen.getByRole("button", {
      name: ButtonText.LoanRequest,
    });
    await user.click(decisionButton);
    const loanModalForm = screen.getByRole("form", { name: "Demande de prêt" });
    const loanRequestParagraph = within(loanModalForm).getByText(/emprunter/i);
    const loanRequestText = `${loan.borrower.username} désire emprunter ${loan.owned_book.book.title} de ${loan.owned_book.book.author_fname} ${loan.owned_book.book.author_lname}`;
    expect(loanRequestParagraph).toBeVisible();
    expect(loanRequestParagraph).toHaveTextContent(loanRequestText);
  });
  it("displays two input elements to allow choosing a date for the start and the desired end of the loan", async () => {
    const user = userEvent.setup();
    const decisionButton = screen.getByRole("button", {
      name: ButtonText.LoanRequest,
    });
    await user.click(decisionButton);
    const loanModalForm = screen.getByRole("form", { name: "Demande de prêt" });
    const loanStartDateInput = within(loanModalForm).getByLabelText(
      "Date de début du prêt",
    );
    const loanEndDateInput = within(loanModalForm).getByLabelText(
      "Date désirée de fin du prêt",
    );
    expect(loanStartDateInput).toBeVisible();
    expect(loanEndDateInput).toBeVisible();
  });
});

describe("modal for confirming book return test suite", () => {
  const loanUpdateType = LoanUpdateType.BookReturn;
  const bookReturnProps = { loan, loanUpdateType };
  beforeEach(() => {
    render(LoanDetails, { props: bookReturnProps });
  });
  afterEach(() => {
    cleanup();
  });
  it("displays a sentence describing that another user returned a book to the user", async () => {
    const user = userEvent.setup();
    const confirmButton = screen.getByRole("button", {
      name: ButtonText.BookReturn,
    });
    await user.click(confirmButton);
    const loanModalForm = screen.getByRole("form", {
      name: "Confirmation de rendu de prêt",
    });
    const bookReturnParagraph = within(loanModalForm).getByText(/avoir rendu/i);
    const bookReturnText = `${loan.borrower.username} déclare vous avoir rendu ${loan.owned_book.book.title} de ${loan.owned_book.book.author_fname} ${loan.owned_book.book.author_lname}`;
    expect(bookReturnParagraph).toBeVisible();
    expect(bookReturnParagraph).toHaveTextContent(bookReturnText);
  });
  it("displays the starting date of the loan and the current date for the book return", async () => {
    const user = userEvent.setup();
    const confirmButton = screen.getByRole("button", {
      name: ButtonText.BookReturn,
    });
    await user.click(confirmButton);
    const loanModalForm = screen.getByRole("form", {
      name: "Confirmation de rendu de prêt",
    });
    const loanStartDate = within(loanModalForm).getByText(
      `${loan.starting_date}`,
      { exact: false },
    );
    const loanEndDate = within(loanModalForm).getByText(`${loan.ending_date}`, {
      exact: false,
    });
    expect(loanStartDate).toBeVisible();
    expect(loanEndDate).toBeVisible();
  });
});
describe("modal for asking book return test suite", async () => {
  const loanUpdateType = LoanUpdateType.AskingBookReturn;
  const props = { loan, loanUpdateType };
  beforeEach(() => {
    render(LoanDetails, { props: props });
  });
  afterEach(() => {
    cleanup();
  });
  it("displays a sentence telling that the user wants to ask for a loaned book to return from another user", async () => {
    const user = userEvent.setup();
    const claimButton = screen.getByRole("button", {
      name: ButtonText.AskingBookReturn,
    });
    await user.click(claimButton);
    const loanModalForm = screen.getByRole("form", {
      name: "Demande de retour de prêt",
    });
    const askingBookReturnParagraph =
      within(loanModalForm).getByText(/souhaitez réclamer/i);
    const askingBookReturnText = `Vous souhaitez réclamer ${loan.owned_book.book.title} de ${loan.owned_book.book.author_fname} ${loan.owned_book.book.author_lname}, prêté à ${loan.borrower.username}`;
    expect(askingBookReturnParagraph).toBeVisible();
    expect(askingBookReturnParagraph).toHaveTextContent(askingBookReturnText);
  });
  it("displays the starting date of the loan and the current date for the book return", async () => {
    const user = userEvent.setup();
    const claimButton = screen.getByRole("button", {
      name: ButtonText.AskingBookReturn,
    });
    await user.click(claimButton);
    const loanModalForm = screen.getByRole("form", {
      name: "Demande de retour de prêt",
    });
    const loanStartDate = within(loanModalForm).getByText(
      `${loan.starting_date}`,
      { exact: false },
    );
    const loanEndDate = within(loanModalForm).getByText(`${loan.ending_date}`, {
      exact: false,
    });
    expect(loanStartDate).toBeVisible();
    expect(loanEndDate).toBeVisible();
  });
});
describe("modal for returning loaned book test suite", () => {
  const loan = require("./fixtures/loans/return_requested.json").loans[0];

  const loanUpdateType = LoanUpdateType.ReturningLoan;
  const props = { loan, loanUpdateType };
  beforeEach(() => {
    render(LoanDetails, { props: props });
  });
  afterEach(() => {
    cleanup();
  });
  it("displays a sentence telling that the user wants to return a book borrowed from another user", async () => {
    const user = userEvent.setup();
    const giveBackButton = screen.getByRole("button", {
      name: ButtonText.ReturningLoan,
    });
    await user.click(giveBackButton);
    const loanModalForm = screen.getByRole("form", {
      name: "Déclaration de rendu de livre emprunté",
    });
    const returningLoanParagraph =
      within(loanModalForm).getByText(/souhaitez rendre/i);
    const returningLoanText = `Vous souhaitez rendre ${loan.owned_book.book.title} de ${loan.owned_book.book.author_fname} ${loan.owned_book.book.author_lname}, emprunté à ${loan.owner.username}`;
    expect(returningLoanParagraph).toBeVisible();
    expect(returningLoanParagraph).toHaveTextContent(returningLoanText);
  });
  it("displays the starting date of the loan and an input to set the loan return date", async () => {
    const user = userEvent.setup();
    const giveBackButton = screen.getByRole("button", {
      name: ButtonText.ReturningLoan,
    });
    await user.click(giveBackButton);
    const loanModalForm = screen.getByRole("form", {
      name: "Déclaration de rendu de livre emprunté",
    });
    const loanStartDate = within(loanModalForm).getByText(
      `${loan.starting_date}`,
      { exact: false },
    );
    const loanEndDate = within(loanModalForm).getByText(`${loan.ending_date}`, {
      exact: false,
    });
    expect(loanStartDate).toBeVisible();
    expect(loanEndDate).toBeVisible();
  });
});
