import {
  afterAll,
  afterEach,
  beforeAll,
  describe,
  expect,
  it,
  vi,
} from "vitest";
import { cleanup, render, screen, within } from "@testing-library/vue";
import userEvent from "@testing-library/user-event";
import * as matchers from "@testing-library/jest-dom/matchers";
import {
  mockNuxtImport,
  registerEndpoint,
  renderSuspended,
} from "@nuxt/test-utils/runtime";

import Component from "~/components/organisms/new-book-search.vue";
import type { Book } from "~/types/boukinApi";

expect.extend(matchers);

const mocks = vi.hoisted(() => {
  return { fetchSearchedBooks: vi.fn(), addBook: vi.fn() };
});
const authenticatedUserId = "3fa85f64-5717-4562-b3fc-2c963f66afa7";
const { useCookieMock } = vi.hoisted(() => {
  return {
    useCookieMock: vi.fn().mockImplementation(() => {
      return {
        value: {
          username: "JujuDu17",
          user_id: authenticatedUserId,
          token: "9876543210",
        },
      };
    }),
  };
});
mockNuxtImport("useCookie", () => {
  return useCookieMock;
});

describe("new book search component static elements test suite", () => {
  beforeAll(() => {
    render(Component);
  });
  afterAll(() => cleanup());

  it("displays a 'Recherche par' heading", () => {
    const bookSearchHeading = screen.getByRole("heading", {
      name: /Recherche par/i,
    });
    expect(bookSearchHeading).toBeInTheDocument();
  });
  it("displays research filter options", () => {
    const isbnFilter = screen.getByRole("radio", {
      name: /isbn/i,
    });
    const titleFilter = screen.getByRole("radio", {
      name: /titre/i,
    });
    const authorFilter = screen.getByRole("radio", {
      name: /auteur/i,
    });
    expect(isbnFilter).toBeInTheDocument();
    expect(titleFilter).toBeInTheDocument();
    expect(authorFilter).toBeInTheDocument();
  });
  it("displays a search bar by default for isbn", () => {
    const isbnSearchBar = screen.getByRole("textbox", {
      name: "isbn",
    });
    expect(isbnSearchBar).toBeInTheDocument();
  });
  it("displays a search button", () => {
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    expect(searchButton).toBeInTheDocument();
  });
  it("hides search's results section as no search has been made", () => {
    const resultsSection = screen.queryByRole("region", {
      name: /résultat/i,
    });
    expect(resultsSection).not.toBeInTheDocument();
  });
});

describe("new book search component search feature test suite", () => {
  afterEach(() => cleanup());

  it("displays the book searched when user fills searchbar with its isbn and presses search button", async () => {
    const user = userEvent.setup();
    const fetchedBooksByIsbnList = require("./fixtures/booksSearch/booksearchbyisbn.json");
    const bookTitle = fetchedBooksByIsbnList[0].title;
    const bookAuthorFName = fetchedBooksByIsbnList[0].author_fname;
    const bookAuthorLName = fetchedBooksByIsbnList[0].author_lname;
    const bookIsbn = fetchedBooksByIsbnList[0].isbn;
    registerEndpoint(`/books/?isbn=${bookIsbn}`, mocks.fetchSearchedBooks);
    mocks.fetchSearchedBooks.mockReturnValue(fetchedBooksByIsbnList);
    await renderSuspended(Component);

    const isbnSearchBar = screen.getByRole("textbox", {
      name: "isbn",
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    await user.type(isbnSearchBar, bookIsbn);
    await user.click(searchButton);

    const searchedBooksList = within(
      screen.getByRole("region", {
        name: /résultat/i,
      }),
    ).getByRole("list");
    expect(within(searchedBooksList).getByText(bookTitle)).toBeInTheDocument();
    expect(
      within(searchedBooksList).getByText(bookAuthorFName, { exact: false }),
    ).toBeInTheDocument();
    expect(
      within(searchedBooksList).getByText(bookAuthorLName, { exact: false }),
    ).toBeInTheDocument();
  });

  it("displays the books searched when user chooses research by title, fills searchbar with partial title and presses search button", async () => {
    const user = userEvent.setup();
    const inputTitle = "hommes";
    const fetchedBooksByTitleList = require("./fixtures/booksSearch/booksearchbytitle.json");
    const bookTitles: string[] = [];
    const bookAuthors: string[] = [];
    fetchedBooksByTitleList.forEach((book: Book) => {
      bookTitles.push(book["title"]);
      bookAuthors.push(book["author_fname"] + " " + book["author_lname"]);
    });
    registerEndpoint(`/books/?title=${inputTitle}`, mocks.fetchSearchedBooks);
    mocks.fetchSearchedBooks.mockReturnValue(fetchedBooksByTitleList);
    await renderSuspended(Component);

    const searchByTitleFilterOption = screen.getByRole("radio", {
      name: /titre/i,
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    await user.click(searchByTitleFilterOption);
    const titleSearchBar = screen.getByRole("textbox", {
      name: "title",
    });
    await user.type(titleSearchBar, inputTitle);
    await user.click(searchButton);

    const searchedBooksList = within(
      screen.getByRole("region", {
        name: /résultat/i,
      }),
    ).getByRole("list");
    const booksSearched = within(searchedBooksList).getAllByRole("listitem");
    for (let index = 0; index < booksSearched.length; index++) {
      expect(
        within(booksSearched[index]).getByText(bookTitles[index]),
      ).toBeInTheDocument();
      expect(
        within(booksSearched[index]).getByText(bookAuthors[index]),
      ).toBeInTheDocument();
    }
  });

  it("displays two search fields when search filter option 'auteur' is selected, to search by author's first name and last name", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);
    const searchByAuthorFilterOption = screen.getByRole("radio", {
      name: /auteur/i,
    });

    await user.click(searchByAuthorFilterOption);

    const authorFirstNameSearchBar = screen.getByRole("textbox", {
      name: "author-fname",
    });
    const authorLastNameSearchBar = screen.getByRole("textbox", {
      name: "author-lname",
    });
    expect(authorFirstNameSearchBar).toBeInTheDocument();
    expect(authorLastNameSearchBar).toBeInTheDocument();
  });

  it("hides search fields by author's first name and last name when search filter option 'auteur' is not selected", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);
    const searchByIsbnFilterOption = screen.getByRole("radio", {
      name: /isbn/i,
    });

    await user.click(searchByIsbnFilterOption);

    const authorFirstNameSearchBar = screen.queryByRole("textbox", {
      name: "author-fname",
    });
    const authorLastNameSearchBar = screen.queryByRole("textbox", {
      name: "author-lname",
    });
    expect(authorFirstNameSearchBar).not.toBeInTheDocument();
    expect(authorLastNameSearchBar).not.toBeInTheDocument();
  });

  it("hides isbn and title search fields when search filter option 'auteur' is selected", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);
    const searchByAuthorFilterOption = screen.getByRole("radio", {
      name: /auteur/i,
    });

    await user.click(searchByAuthorFilterOption);

    const isbnSearchBar = screen.queryByRole("textbox", {
      name: "isbn",
    });
    const titleSearchBar = screen.queryByRole("textbox", {
      name: "title",
    });
    expect(isbnSearchBar).not.toBeInTheDocument();
    expect(titleSearchBar).not.toBeInTheDocument();
  });

  it("displays the books searched with 'Ajouter' button when user chooses research by author, fills searchbars with exact firstname and lastname and presses search button", async () => {
    const user = userEvent.setup();
    const inputFirstname = "Tal";
    const inputLastname = "Madesta";
    const fetchedBooksByAuthorList = require("./fixtures/booksSearch/booksearchbyauthor.json");
    const bookTitles: string[] = [];
    const bookAuthors: string[] = [];
    fetchedBooksByAuthorList.forEach((book: Book) => {
      bookTitles.push(book["title"]);
      bookAuthors.push(book["author_fname"] + " " + book["author_lname"]);
    });
    registerEndpoint(
      `/books/?author_lname=${inputLastname}&author_fname=${inputFirstname}`,
      mocks.fetchSearchedBooks,
    );
    mocks.fetchSearchedBooks.mockReturnValue(fetchedBooksByAuthorList);
    await renderSuspended(Component);

    const searchByAuthorFilterOption = screen.getByRole("radio", {
      name: /auteur/i,
    });
    await user.click(searchByAuthorFilterOption);
    const authorFirstNameSearchBar = screen.getByRole("textbox", {
      name: "author-fname",
    });
    const authorLastNameSearchBar = screen.getByRole("textbox", {
      name: "author-lname",
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    await user.type(authorFirstNameSearchBar, inputFirstname);
    await user.type(authorLastNameSearchBar, inputLastname);
    await user.click(searchButton);

    const searchedBooksList = within(
      screen.getByRole("region", {
        name: /résultat/i,
      }),
    ).getByRole("list");
    const booksSearched = within(searchedBooksList).getAllByRole("listitem");
    for (let index = 0; index < booksSearched.length; index++) {
      expect(
        within(booksSearched[index]).getByText(bookTitles[index]),
      ).toBeInTheDocument();
      expect(
        within(booksSearched[index]).getByText(bookAuthors[index]),
      ).toBeInTheDocument();
      expect(
        within(booksSearched[index]).getByRole("button", { name: /Ajouter/i }),
      ).toBeInTheDocument();
    }
  });

  it("fetches API url with only 'author_lname' query parameter when user made a research by author exact last name only", async () => {
    mocks.fetchSearchedBooks.mockClear();
    const user = userEvent.setup();
    const inputLastname = "Madesta";
    const searchByAuthorLastnameUrl = `/books/?author_lname=${inputLastname}`;
    registerEndpoint(searchByAuthorLastnameUrl, mocks.fetchSearchedBooks);
    await renderSuspended(Component);

    const searchByAuthorFilterOption = screen.getByRole("radio", {
      name: /auteur/i,
    });
    await user.click(searchByAuthorFilterOption);
    const authorFirstNameSearchBar = screen.getByRole("textbox", {
      name: "author-fname",
    });
    const authorLastNameSearchBar = screen.getByRole("textbox", {
      name: "author-lname",
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    await user.clear(authorFirstNameSearchBar);
    await user.type(authorLastNameSearchBar, inputLastname);
    await user.click(searchButton);

    expect(mocks.fetchSearchedBooks).toHaveBeenCalledOnce();
  });

  it("displays a message when book's research returns no result (status 404)", async () => {
    const user = userEvent.setup();
    const noBookIsbn = "0000000000000";
    registerEndpoint(`/books/?isbn=${noBookIsbn}`, mocks.fetchSearchedBooks);
    mocks.fetchSearchedBooks.mockReturnValue(
      new Response(null, { status: 404 }),
    );
    await renderSuspended(Component);

    const isbnSearchBar = screen.getByRole("textbox", {
      name: "isbn",
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    await user.type(isbnSearchBar, noBookIsbn);
    await user.click(searchButton);

    const messageNotFound = screen.getByText(
      /Nous n'avons pas trouvé de livre correspondant à votre recherche/i,
    );
    expect(messageNotFound).toBeInTheDocument();
  });

  it("displays an error message when book's research fails", async () => {
    const user = userEvent.setup();
    const noBookIsbn = "0000000000000";
    const apiErrorResponse = { message: "Error" };
    registerEndpoint(`/books/?isbn=${noBookIsbn}`, mocks.fetchSearchedBooks);
    mocks.fetchSearchedBooks.mockRejectedValue(apiErrorResponse);
    await renderSuspended(Component);

    const isbnSearchBar = screen.getByRole("textbox", {
      name: "isbn",
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    await user.type(isbnSearchBar, noBookIsbn);
    await user.click(searchButton);

    const messageNotFound = screen.getByText(
      /Une erreur s'est produite lors de la recherche/i,
    );
    expect(messageNotFound).toBeInTheDocument();
  });

  it("allows user doing another book research after a failure (status 404)", async () => {
    const user = userEvent.setup();
    const noBookIsbn = "0000000000000";
    mocks.fetchSearchedBooks.mockReturnValueOnce(
      new Response(null, { status: 404 }),
    );
    registerEndpoint(`/books/?isbn=${noBookIsbn}`, mocks.fetchSearchedBooks);
    const fetchedBooksByIsbn = require("./fixtures/booksSearch/booksearchbyisbn.json");
    const existingBookIsbn = fetchedBooksByIsbn[0]["isbn"];
    mocks.fetchSearchedBooks.mockReturnValueOnce(fetchedBooksByIsbn);
    registerEndpoint(
      `/books/?isbn=${existingBookIsbn}`,
      mocks.fetchSearchedBooks,
    );
    await renderSuspended(Component);

    const isbnSearchBar = screen.getByRole("textbox", {
      name: "isbn",
    });
    const searchButton = screen.getByRole("button", {
      name: /rechercher/i,
    });
    // First research wich returns no result
    await user.type(isbnSearchBar, noBookIsbn);
    await user.click(searchButton);
    const messageNotFound = screen.getByText(
      /Nous n'avons pas trouvé de livre correspondant à votre recherche/i,
    );
    expect(messageNotFound).toBeInTheDocument();
    // Second research which should display results
    await user.clear(isbnSearchBar);
    await user.type(isbnSearchBar, existingBookIsbn);
    await user.click(searchButton);

    expect(messageNotFound).not.toBeInTheDocument();
    const searchedBookTitle = screen.getByText(fetchedBooksByIsbn[0]["title"]);
    expect(searchedBookTitle).toBeInTheDocument();
  });
});
