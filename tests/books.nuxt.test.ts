import {
  afterAll,
  afterEach,
  beforeAll,
  describe,
  expect,
  it,
  vi,
} from "vitest";
import { cleanup, screen, within } from "@testing-library/vue";
import * as matchers from "@testing-library/jest-dom/matchers";
import userEvent from "@testing-library/user-event";
import {
  mockNuxtImport,
  registerEndpoint,
  renderSuspended,
} from "@nuxt/test-utils/runtime";

import BookCollectionPage from "@/pages/books/index.vue";
import type { OwnedBook } from "~/types/boukinApi";

expect.extend(matchers);

const authenticatedUserId = "3fa85f64-5717-4562-b3fc-2c963f66afa7";
const { useCookieMock } = vi.hoisted(() => {
  return {
    useCookieMock: vi.fn().mockImplementation(() => {
      return {
        value: {
          email: "juju@test.fr",
          username: "JujuDu17",
          user_id: authenticatedUserId,
          token: "9876543210",
        },
      };
    }),
  };
});
mockNuxtImport("useCookie", () => {
  return useCookieMock;
});

describe("book collection page static elements test suite", () => {
  beforeAll(async () => {
    await renderSuspended(BookCollectionPage);
  });
  afterAll(() => cleanup());

  it("displays a 'Ma bibliothèque' heading", () => {
    const myBookCollectionHeading = screen.getByRole("heading", {
      name: "Ma bibliothèque",
    });
    expect(myBookCollectionHeading).toBeInTheDocument();
  });

  it("displays a 'Ajouter un livre' link", () => {
    const addBookLink = screen.getByRole("link", {
      name: /ajouter un livre/i,
    });
    expect(addBookLink).toBeInTheDocument();
  });

  it("displays books filter options", () => {
    const allBooksFilter = screen.getByRole("radio", {
      name: /tous/i,
    });
    const loanedBooksFilter = screen.getByRole("radio", {
      name: /prêtés/i,
    });
    const availableBooksFilter = screen.getByRole("radio", {
      name: /disponibles/i,
    });
    const notvisibleBooksFilter = screen.getByRole("radio", {
      name: /cachés/i,
    });
    expect(allBooksFilter).toBeInTheDocument();
    expect(loanedBooksFilter).toBeInTheDocument();
    expect(availableBooksFilter).toBeInTheDocument();
    expect(notvisibleBooksFilter).toBeInTheDocument();
  });
});

describe("book collection page dynamic elements test suite", () => {
  const mocks = vi.hoisted(() => {
    return { fetchOwnedbooks: vi.fn() };
  });

  registerEndpoint("/owned-books/", mocks.fetchOwnedbooks);

  afterEach(() => cleanup());

  it("displays a list of all ownedbooks regardless of their status with title and author when sucessfully fetched", async () => {
    const fetchedOwnedbooksList = require("./fixtures/books/multipleownedbooks.json");
    mocks.fetchOwnedbooks.mockReturnValue(fetchedOwnedbooksList);
    await renderSuspended(BookCollectionPage);

    const bookTitles: string[] = [];
    const bookAuthors: string[] = [];
    fetchedOwnedbooksList.forEach((book: OwnedBook) => {
      bookTitles.push(book["book"]["title"]);
      bookAuthors.push(
        book["book"]["author_fname"] + " " + book["book"]["author_lname"],
      );
    });

    const ownedbooksList = screen.getByRole("list", {
      name: "Liste des livres possédés",
    });
    const ownedbooksItems = within(ownedbooksList).getAllByRole("listitem");

    for (let index = 0; index < ownedbooksItems.length; index++) {
      expect(
        within(ownedbooksItems[index]).getByText(bookTitles[index]),
      ).toBeInTheDocument();
      expect(
        within(ownedbooksItems[index]).getByText(bookAuthors[index]),
      ).toBeInTheDocument();
    }
  });

  const booksDisplayByFilterOptionScenarios = [
    { filterName: "prêtés", bookStatus: "loaned" },
    { filterName: "disponibles", bookStatus: "available" },
    { filterName: "cachés", bookStatus: "not visible" },
  ];
  booksDisplayByFilterOptionScenarios.forEach(({ filterName, bookStatus }) => {
    it(`displays only ${bookStatus} books when corresponding option is selected`, async () => {
      const user = userEvent.setup();
      const fetchedOwnedbooksList = require("./fixtures/books/multipleownedbooks.json");
      mocks.fetchOwnedbooks.mockReturnValue(fetchedOwnedbooksList);
      await renderSuspended(BookCollectionPage);

      const BooksFilter = screen.getByRole("radio", {
        name: filterName,
      });
      await user.click(BooksFilter);

      const ownedbooksList = screen.getByRole("list", {
        name: "Liste des livres possédés",
      });
      fetchedOwnedbooksList.forEach((book: OwnedBook) => {
        if (book.status == bookStatus) {
          expect(
            within(ownedbooksList).getByText(book.book.title),
          ).toBeInTheDocument();
        } else {
          expect(
            within(ownedbooksList).queryByText(book.book.title),
          ).not.toBeInTheDocument();
        }
      });
    });
  });

  it("displays an error message if ownedbooks data fetching failed", async () => {
    mocks.fetchOwnedbooks.mockReturnValue(Promise.reject({ data: "error" }));
    await renderSuspended(BookCollectionPage);
    const error = screen.getByText(/Erreur de chargement des livres possédés/i);
    expect(error).toBeInTheDocument();
  });

  const emptyOwnedbookListMessageScenarios = [
    { option: "tous", message: "Vous ne possédez aucun livre." },
    {
      option: "prêtés",
      message: "Aucun de vos livres n'est en cours de prêt.",
    },
    {
      option: "disponibles",
      message: "Vous n'avez aucun livre disponible à prêter.",
    },
    { option: "cachés", message: "Vous n'avez aucun livre caché." },
  ];
  emptyOwnedbookListMessageScenarios.forEach(({ option, message }) => {
    it(`displays an informative message when user has no corresponding book to selected filter option: '${option}'`, async () => {
      const user = userEvent.setup();
      mocks.fetchOwnedbooks.mockResolvedValue([]);
      await renderSuspended(BookCollectionPage);
      const filterOption = screen.getByRole("radio", { name: option });
      const ownedbooksList = screen.queryByRole("list", {
        name: "Liste des livres possédés",
      });

      await user.click(filterOption);

      expect(screen.getByText(message)).toBeInTheDocument();
      expect(ownedbooksList).not.toBeInTheDocument();
    });
  });
});
