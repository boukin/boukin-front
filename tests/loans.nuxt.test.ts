import { afterEach, beforeEach, expect, describe, it, vi } from "vitest";
import { cleanup, screen, within } from "@testing-library/vue";
import * as matchers from "@testing-library/jest-dom/matchers";
import LoansPage from "../pages/loans/index.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import type { Loan } from "~/types/loan";
expect.extend(matchers);

const twoLoans = require("./fixtures/loans/twoloans.json");
const userId = "3c48ecd4-d0bd-4171-a034-4444b768fbd8";
const mocks = vi.hoisted(() => {
  return { mockFetchLoans: vi.fn() };
});
registerEndpoint(`/users/${userId}/loans/`, mocks.mockFetchLoans);
mockNuxtImport("useCookie", () => {
  return () => {
    return {
      value: { email: "wowilovebooks@boukin.org", user_id: userId },
    };
  };
});
mocks.mockFetchLoans.mockReturnValue(twoLoans);

describe("loans page static elements test suite", () => {
  beforeEach(async () => {
    await renderSuspended(LoansPage);
  });
  afterEach(() => cleanup());

  it("displays a 'Mes prêts' heading", async () => {
    const myLoansHeading = screen.getByRole("heading", { name: "Mes prêts" });
    expect(myLoansHeading).toBeInTheDocument();
  });

  it("displays a 'Notifications de prêts' heading", async () => {
    const loansNotifications = screen.getByRole("heading", {
      name: "Notifications de prêts",
    });
    expect(loansNotifications).toBeInTheDocument();
  });

  it("displays a 'Prêts en cours' heading", async () => {
    const activeLoansHeading = screen.getByRole("heading", {
      name: "Prêts en cours",
    });
    expect(activeLoansHeading).toBeInTheDocument();
  });

  it("displays a 'Nouveau prêt' button", async () => {
    const newLoanHeading = screen.getByRole("button", { name: "Nouveau prêt" });
    expect(newLoanHeading).toBeInTheDocument();
  });
});

describe("loans page dynamic elements test suite", () => {
  const loan = require("./fixtures/loans/loan_active.json");
  const multipleLoans = require("./fixtures/loans/multiplesloans.json");
  const returnRequested = require("./fixtures/loans/return_requested.json");
  const returnConfirmationPending = require("./fixtures/loans/return_confirmation_pending.json");
  const loanRequest = require("./fixtures/loans/loan_requested.json");

  afterEach(() => {
    cleanup();
  });

  it("displays a heading showing the current ongoing loans", async () => {
    mocks.mockFetchLoans.mockReturnValue(loan);

    await renderSuspended(LoansPage);

    const oneOngoingLoan = screen.getByText("Vous avez un prêt en cours.");

    expect(oneOngoingLoan).toBeInTheDocument();

    cleanup();
    mocks.mockFetchLoans.mockReturnValue(twoLoans);
    await renderSuspended(LoansPage);

    const ongoingLoans = screen.getByText("Vous avez 2 prêts en cours.");
    expect(ongoingLoans).toBeInTheDocument();
  });
  it("displays a list of loaned books with associated user when loaned books are successfully fetched", async () => {
    mocks.mockFetchLoans.mockReturnValue(twoLoans);
    const bookTitles: string[] = [];
    const bookBorrowers: string[] = [];
    twoLoans["loans"].forEach(function (loan: Loan) {
      bookTitles.push(loan["owned_book"]["book"]["title"]);
      bookBorrowers.push(loan["borrower"]["username"]);
    });

    await renderSuspended(LoansPage);

    const loansList = screen.getByRole("list", { name: "Liste de prêts" });
    const { getAllByRole } = within(loansList);
    const loansItems = getAllByRole("listitem");
    for (let index = 0; index < loansItems.length; index++) {
      expect(loansItems[index]).toHaveTextContent(
        `${bookTitles[index]}, prêté à ${bookBorrowers[index]}`,
      );
    }
  });

  it("displays a message to the user saying that no loans data were found on the server side", async () => {
    const noLoansFound = new Response(null, { status: 404 });
    mocks.mockFetchLoans.mockReturnValue(noLoansFound);
    await renderSuspended(LoansPage);
    const error = screen.getByText(/Vous n'avez pas de prêts en cours/i);

    expect(error).toBeInTheDocument();
  });

  it("displays an error to the user if there was an error on the client side when attempting to fetch loans", async () => {
    mocks.mockFetchLoans.mockRejectedValue({ data: "error" });
    await renderSuspended(LoansPage);
    const error = screen.getByText(/Erreur de chargement des prêts/i);

    expect(error).toBeInTheDocument();
  });

  it("displays a list of loaned books indicating if the books were either loaned by or loaned to the user", async () => {
    mocks.mockFetchLoans.mockReturnValue(multipleLoans);
    const loanedBookTitles: string[] = [];
    const borrowedBookTitles: string[] = [];
    multipleLoans["loans"].forEach(function (loan: Loan) {
      if (loan.owner.user_id == userId) {
        loanedBookTitles.push(loan.owned_book.book.title);
      } else {
        borrowedBookTitles.push(loan.owned_book.book.title);
      }
    });
    await renderSuspended(LoansPage);

    const loansList = screen.getByRole("list", { name: "Liste de prêts" });
    const { getAllByRole } = within(loansList);
    const loansItems = getAllByRole("listitem");

    for (let index = 0; index < loanedBookTitles.length; index++) {
      expect(loansItems[index]).toHaveTextContent(
        `${loanedBookTitles[index]}, prêté à wowiborrowbooks`,
      );
    }
    for (let index = 0; index < borrowedBookTitles.length; index++) {
      expect(loansItems[index + loanedBookTitles.length]).toHaveTextContent(
        `${borrowedBookTitles[index]}, emprunté à wowiborrowbooks`,
      );
    }
  });
  it("displays a notification to the user  that they are in waiting of ackownledgement of a return request from another user", async () => {
    mocks.mockFetchLoans.mockReturnValue(returnRequested);

    const returnRequestsAsOwner: Loan[] = returnRequested.loans.filter(
      (loan: Loan) => loan.owner.user_id == userId,
    );
    await renderSuspended(LoansPage);

    const borrowerUsername = returnRequestsAsOwner[0].borrower.username;
    const bookTitle = returnRequestsAsOwner[0].owned_book.book.title;
    const notificationsList = screen.getByRole("list", {
      name: "Liste de notifications",
    });
    const { getAllByRole } = within(notificationsList);
    const notificationItems = getAllByRole("listitem");
    const waitingIcon = screen.getByRole("img", {
      name: "Icône d'attente de retour d'un livre prêté",
    });
    expect(notificationItems[1]).toHaveTextContent(
      `En attente de retour de ${bookTitle}, prêté à ${borrowerUsername}`,
    );
    expect(waitingIcon).toBeVisible();
  });
  it("displays a notification to the user when a loaned book has been claimed by its owner", async () => {
    mocks.mockFetchLoans.mockReturnValue(returnRequested);
    await renderSuspended(LoansPage);

    const ownerUsername = returnRequested["loans"][0]["owner"]["username"];
    const bookTitle =
      returnRequested["loans"][0]["owned_book"]["book"]["title"];
    const notificationsList = screen.getByRole("list", {
      name: "Liste de notifications",
    });
    const { getAllByRole } = within(notificationsList);
    const notificationsItems = getAllByRole("listitem");
    const warningIcon = screen.getByRole("img", {
      name: "Icône d'avertissement",
    });
    expect(notificationsItems[0]).toHaveTextContent(
      `${ownerUsername} souhaite récupérer ${bookTitle}`,
    );
    expect(warningIcon).toBeVisible();
  });
  it("displays a notification to the user when a friend has declared to have returned a loaned book to her/him", async () => {
    mocks.mockFetchLoans.mockReturnValue(returnConfirmationPending);
    await renderSuspended(LoansPage);

    const borrowerUsername =
      returnConfirmationPending["loans"][0]["borrower"]["username"];
    const bookTitle =
      returnConfirmationPending["loans"][0]["owned_book"]["book"]["title"];
    const notificationsList = screen.getByRole("list", {
      name: "Liste de notifications",
    });
    const { getAllByRole } = within(notificationsList);
    const notificationsItems = getAllByRole("listitem");
    expect(notificationsItems[0]).toHaveTextContent(
      `${borrowerUsername} a déclaré vous avoir rendu ${bookTitle}`,
    );
  });
  it("displays a notification to the user when a friend wants to borrow one of his/her books", async () => {
    mocks.mockFetchLoans.mockReturnValue(loanRequest);
    await renderSuspended(LoansPage);

    const borrowerUsername = loanRequest["loans"][0]["borrower"]["username"];
    const bookTitle = loanRequest["loans"][0]["owned_book"]["book"]["title"];
    const notificationsList = screen.getByRole("list", {
      name: "Liste de notifications",
    });
    const { getAllByRole } = within(notificationsList);
    const notificationsItems = getAllByRole("listitem");
    expect(notificationsItems[0]).toHaveTextContent(
      `${borrowerUsername} désire emprunter ${bookTitle}`,
    );
  });
  it("displays a message to the user saying that no notifications are to be displayed when no loans data were found on the server side", async () => {
    const noNotifications = new Response(null, { status: 404 });
    mocks.mockFetchLoans.mockResolvedValue(noNotifications);
    await renderSuspended(LoansPage);
    const noNotificationsMessage = screen.getByText(
      /Vous n'avez pas de notifications/i,
    );

    expect(noNotificationsMessage).toBeInTheDocument();
  });
  it("displays a message to the user saying that no notifications are to be displayed when no notifications are present", async () => {
    await renderSuspended(LoansPage);
    const noNotificationsMessage = screen.getByText(
      /Vous n'avez pas de notifications/i,
    );

    expect(noNotificationsMessage).toBeInTheDocument();
  });
  it("displays a message to the user saying that no notifications are to be displayed when no active loans were found", async () => {
    mocks.mockFetchLoans.mockResolvedValue({});
    await renderSuspended(LoansPage);
    const noLoansMessage = screen.getByText(/Vous n'avez pas de prêts/i);

    expect(noLoansMessage).toBeInTheDocument();
  });
  it("displays a message to the user indicating the absence of notifications when there was an error on the client side", async () => {
    const anotherError = new Response(null, { status: 500 });
    mocks.mockFetchLoans.mockResolvedValue(anotherError);
    await renderSuspended(LoansPage);
    const error = screen.getByText(/Erreur de chargement des notifications/i);
    expect(error).toBeInTheDocument();
  });
});
