import { cleanup, screen, within } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../pages/friends/index.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import type { Friendship } from "../types/friendship.ts";
expect.extend(matchers);

describe("friends page static elements test suite", () => {
  beforeEach(async () => {
    await renderSuspended(Component);
  });
  afterEach(() => cleanup());

  it("displays a 'Mes ami·e·s' heading", () => {
    const friendsPageHeading = screen.getByRole("heading", {
      name: "Mes ami·e·s",
    });
    expect(friendsPageHeading).toBeInTheDocument();
  });
  it("displays a 'Ajouter un·e ami·e' link on desktop view", () => {
    const addFriendLink = screen.getByRole("link", {
      name: /Ajouter un·e ami·e/i,
    });
    expect(addFriendLink).toBeInTheDocument();
    expect(addFriendLink).toHaveAttribute("href", "/friends/search");
  });

  it('displays icon and text on desktop view for "Ajouter un ami" link', () => {
    const icon = document.querySelector("img[aria-hidden='true']");
    const text = screen.getByText("Ajouter un·e ami·e");

    expect(icon).toBeInTheDocument();
    expect(text).toBeInTheDocument();
  });

  it("displays a 'Mes invitations' heading", () => {
    const friendsRequestSectionHeading = screen.getByRole("heading", {
      name: "Mes invitations en attente :",
    });
    expect(friendsRequestSectionHeading).toBeInTheDocument();
  });
  it("displays a 'Ma liste d'ami·e·s", () => {
    const friendsListSectionHeading = screen.getByRole("heading", {
      name: "Ma liste d'ami·e·s :",
    });
    expect(friendsListSectionHeading).toBeInTheDocument();
  });
});

describe("friends page dynamic elements test suite", () => {
  const friendships = require("./fixtures/friendships.json");
  const activeFriendships = friendships.filter(
    (friendship: Friendship) =>
      friendship.status === "friendship_active" &&
      friendship.friend.role !== "external",
  );
  const friendshipRequests = friendships.filter(
    (friendship: Friendship) =>
      friendship.status === "friendship_requested" &&
      friendship.friend.is_requesting_user,
  );

  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: {
            email: "Camilledu91@wanadoo.fr",
            user_id: "1234567890",
            token: "9876543210",
            username: "camille",
          },
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  const mocks = vi.hoisted(() => {
    return {
      getTokenUserId: vi.fn(),
      fetchFriends: vi.fn(),
    };
  });

  const user_id = "1234567890";
  mocks.getTokenUserId.mockReturnValue(user_id);
  mocks.fetchFriends.mockReturnValue(friendships);
  registerEndpoint(`/users/${user_id}/friendships/`, mocks.fetchFriends);

  afterEach(() => cleanup());

  it("displays a list of friends when friendships are successfully fetched", async () => {
    mocks.fetchFriends.mockReturnValue(friendships);

    await renderSuspended(Component);

    const friendsList = screen.getByRole("list", { name: "Liste d'amis" });
    const listItem = within(friendsList).getAllByRole("listitem");

    // Vérifie qu'on a bien la liste d'amis
    expect(friendsList).toBeInTheDocument();
    // Vérifie qu'elle fait bien la même taille que prévu
    expect(listItem.length).toBe(activeFriendships.length);
    // Vérifie que le username est bien affiché
    listItem.forEach((item, index) => {
      const expectedUsername = activeFriendships[index].friend.username;
      expect(item).toHaveTextContent(expectedUsername);
    });
  });

  it("displays a list of friendship requests when friendships are successfully fetched ", async () => {
    mocks.fetchFriends.mockReturnValue(friendships);

    await renderSuspended(Component);

    const friendRequests = screen.getByRole("list", {
      name: "Demandes d'amitié",
    });
    const listItem = within(friendRequests).getAllByRole("listitem");

    // Vérifie qu'on a bien la liste d'amis
    expect(friendRequests).toBeInTheDocument();
    // Vérifie qu'elle fait bien la même taille que prévu
    expect(listItem.length).toBe(friendshipRequests.length);
    // Vérifie que le username est bien affiché
    listItem.forEach((item, index) => {
      const expectedUsername = friendshipRequests[index].friend.username;
      expect(item).toHaveTextContent(expectedUsername);
    });
  });

  it("displays an error to the user if no friends data has been succesfully fetched", async () => {
    mocks.fetchFriends.mockReturnValue(Promise.reject({ data: "error" }));

    await renderSuspended(Component);

    const error = screen.getByText(/Aucune amitié n'a été trouvée./i);

    expect(error).toBeInTheDocument();
  });
});
