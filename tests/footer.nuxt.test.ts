import { cleanup, render, screen, within } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Footer from "../components/organisms/footer.vue";

expect.extend(matchers);

describe("footer component test suite", () => {
  beforeEach(() => {
    render(Footer);
  });
  afterEach(() => {
    cleanup();
  });

  it("displays a loans section with a heading and the corresponding links", () => {
    const loansHeading = screen.getByRole("heading", { name: "Prêts" });
    const myLoansLink = screen.getByRole("link", { name: "Mes prêts" });
    const newLoanLink = screen.getByRole("link", { name: "Nouveau prêt" });

    expect(loansHeading).toBeVisible();
    expect(myLoansLink).toBeVisible();
    expect(newLoanLink).toBeVisible();
  });
  it("displays a book collection section with a heading and the corresponding links", () => {
    const bookCollectionHeading = screen.getByRole("heading", {
      name: "Bibliothèque",
    });
    const myBooksLink = screen.getByRole("link", { name: "Ma bibliothèque" });
    const addABookLink = screen.getByRole("link", { name: "Ajouter un livre" });

    expect(bookCollectionHeading).toBeVisible();
    expect(myBooksLink).toBeVisible();
    expect(addABookLink).toBeVisible();
  });
  it("displays a friends section with a heading and the corresponding links", () => {
    const friendsHeading = screen.getByRole("heading", {
      name: "Ami-e-s",
    });
    const myFriendsLink = screen.getByRole("link", { name: "Mes ami-e-s" });
    const addAFriendLink = screen.getByRole("link", {
      name: "Ajouter un-e ami-e",
    });

    expect(friendsHeading).toBeVisible();
    expect(myFriendsLink).toBeVisible();
    expect(addAFriendLink).toBeVisible();
  });
  it("displays an account section with a heading and the corresponding links", () => {
    const accountHeading = screen.getByRole("heading", {
      name: "Compte",
    });
    const myAccountLink = screen.getByRole("link", { name: "Mon compte" });

    expect(accountHeading).toBeVisible();
    expect(myAccountLink).toBeVisible();
  });
  it("displays a legal mentions with a heading and the corresponding links", () => {
    const legalAndPoliciesHeading = screen.getByRole("heading", {
      name: "Mentions légales",
    });
    expect(legalAndPoliciesHeading).toBeVisible();
  });
});
