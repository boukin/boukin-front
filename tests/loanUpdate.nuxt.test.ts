import LoanUpdate from "../components/molecules/loan-update.vue";
import { afterEach, expect, describe, it, vi } from "vitest";
import { cleanup, render, screen } from "@testing-library/vue";
import userEvent from "@testing-library/user-event";
import { registerEndpoint, mockNuxtImport } from "@nuxt/test-utils/runtime";
import * as matchers from "@testing-library/jest-dom/matchers";
import { ButtonText, LoanUpdateType, LoanUpdateMessage } from "../types/loan";
expect.extend(matchers);

const today = new Date();
const one_week_later = new Date();
one_week_later.setDate(today.getDate() + 7);
const today_iso8601 = today.toISOString().substring(0, 10);
const one_week_later_iso8601 = one_week_later.toISOString().substring(0, 10);
const loanId = "4dba81f8-a7af-4676-a62a-c873d8c7ad6d";
const userId = "3c48ecd4-d0bd-4171-a034-4444b768fbd8";
const token = "9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b";

describe("loanUpdate types test suite", () => {
  afterEach(() => cleanup());
  const dates = {
    starting_date: today_iso8601,
    ending_date: one_week_later_iso8601,
  };
  it("displays two buttons to allow or deny a loan request", async () => {
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.LoanRequest,
        dates: dates,
        loanId: loanId,
      },
    });
    const allowButton = screen.getByRole("button", {
      name: ButtonText.AcceptLoanRequest,
    });
    const denyButton = screen.getByRole("button", {
      name: ButtonText.DenyLoanRequest,
    });
    expect(allowButton).toBeVisible();
    expect(denyButton).toBeVisible();
  });
  it("displays a button to ask for a book", async () => {
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.AskingBookReturn,
        loanId: loanId,
      },
    });
    const askButton = screen.getByRole("button", {
      name: ButtonText.AskingBookReturn,
    });
    expect(askButton).toBeVisible();
  });
  it("displays a button to give back a book", async () => {
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.BookReturn,
        loanId: loanId,
      },
    });
    const giveBackButton = screen.getByRole("button", {
      name: ButtonText.BookReturn,
    });
    expect(giveBackButton).toBeVisible();
  });
});
describe("button with API calls testing suite", () => {
  const dates = {
    starting_date: today_iso8601,
    ending_date: one_week_later_iso8601,
  };
  afterEach(() => cleanup());
  mockNuxtImport("useCookie", () => {
    return () => {
      return {
        value: {
          email: "wowilovebooks@boukin.org",
          user_id: userId,
          token: token,
        },
      };
    };
  });
  const mocks = vi.hoisted(() => {
    return { postUpdateLoanStatus: vi.fn() };
  });

  const apiSuccessfulResponse = new Response(null, { status: 204 });
  registerEndpoint(`/loans/${loanId}/`, mocks.postUpdateLoanStatus);

  it("displays a message acknowledging the loan request status update and the set loan dates after pressing the button and hides the button", async () => {
    mocks.postUpdateLoanStatus.mockReturnValue(apiSuccessfulResponse);
    const user = userEvent.setup();
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.LoanRequest,
        dates: dates,
        loanId: loanId,
      },
    });
    const allowButton = screen.getByRole("button", {
      name: ButtonText.AcceptLoanRequest,
    });
    const denyButton = screen.getByRole("button", {
      name: ButtonText.DenyLoanRequest,
    });
    await user.click(allowButton);
    const activeLoanMessage = screen.getByText(LoanUpdateMessage.Active);
    const loanStartDateMessage = screen.getByText(
      `Le prêt commence le ${today_iso8601}.`,
    );
    const loanEndDateMessage = screen.getByText(
      `Il se termine le ${one_week_later_iso8601}.`,
    );
    expect(activeLoanMessage).toBeVisible();
    expect(loanStartDateMessage).toBeVisible();
    expect(loanEndDateMessage).toBeVisible();
    expect(allowButton).not.toBeVisible();
    expect(denyButton).not.toBeVisible();
  });
  it("displays an error message if the loan request status update has failed on the server side after pressing the button", async () => {
    const user = userEvent.setup();
    const error = new Response(null, { status: 404 });
    mocks.postUpdateLoanStatus.mockResolvedValue(error);
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.LoanRequest,
        loanId: loanId,
      },
    });
    const allowButton = screen.getByRole("button", {
      name: ButtonText.AcceptLoanRequest,
    });
    await user.click(allowButton);
    const errorMessage = screen.getByText(/404/i);
    expect(errorMessage).toBeVisible();
  });
  it("displays a message after pressing the button and denying a loan request to another user and hides the button", async () => {
    const user = userEvent.setup();
    mocks.postUpdateLoanStatus.mockResolvedValue(apiSuccessfulResponse);
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.LoanRequest,
        loanId: loanId,
      },
    });
    const denyButton = screen.getByRole("button", {
      name: ButtonText.DenyLoanRequest,
    });
    await user.click(denyButton);
    const loanDeniedMessage = screen.getByText(LoanUpdateMessage.Denied);
    expect(loanDeniedMessage).toBeVisible();
    expect(denyButton).not.toBeVisible();
  });
  it("displays a message after pressing the button and asking for the return of a loan to another user and hides the button", async () => {
    const user = userEvent.setup();
    mocks.postUpdateLoanStatus.mockResolvedValue(apiSuccessfulResponse);
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.AskingBookReturn,
        loanId: loanId,
      },
    });
    const claimButton = screen.getByRole("button", {
      name: ButtonText.AskingBookReturn,
    });
    await user.click(claimButton);
    const returnRequestedMessage = screen.getByText(
      LoanUpdateMessage.ReturnRequested,
    );
    expect(returnRequestedMessage).toBeVisible();
    expect(claimButton).not.toBeVisible();
  });
  it("displays a message after pressing the button and asking for confirmation for a book loaned by another user to the user and hides the button", async () => {
    const user = userEvent.setup();
    mocks.postUpdateLoanStatus.mockResolvedValue(apiSuccessfulResponse);
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.ReturningLoan,
        loanId: loanId,
      },
    });
    const giveBackButton = screen.getByRole("button", {
      name: ButtonText.ReturningLoan,
    });
    await user.click(giveBackButton);
    const returnConfirmationPendingMessage = screen.getByText(
      LoanUpdateMessage.ReturnConfirmationPending,
    );
    expect(returnConfirmationPendingMessage).toBeVisible();
    expect(giveBackButton).not.toBeVisible();
  });
  it("displays a message after pressing the button and confirming that a loaned book was returned to the user and hides the button", async () => {
    const user = userEvent.setup();
    mocks.postUpdateLoanStatus.mockResolvedValue(apiSuccessfulResponse);
    render(LoanUpdate, {
      props: {
        loanUpdateType: LoanUpdateType.BookReturn,
        loanId: loanId,
      },
    });
    const confirmButton = screen.getByRole("button", {
      name: ButtonText.BookReturn,
    });
    await user.click(confirmButton);
    const returnConfirmationPendingMessage = screen.getByText(
      LoanUpdateMessage.ReturnConfirmed,
    );
    expect(returnConfirmationPendingMessage).toBeVisible();
    expect(confirmButton).not.toBeVisible();
  });
});
