import { cleanup, render, screen, within } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import NavbarComponent from "../components/organisms/navbar.vue";

expect.extend(matchers);

describe("navbar component test suite", () => {
  let navbar: ReturnType<typeof within>;
  beforeEach(() => {
    render(NavbarComponent);
    navbar = within(screen.getByLabelText("Barre de navigation web"));
  });
  afterEach(() => cleanup());

  it("displays a 'Mes prêts' link", () => {
    expect(navbar.getByRole("link", { name: "Mes prêts" })).toBeInTheDocument();
  });

  it("displays a 'Mes ami-e-s' link", () => {
    const friendsLink = navbar.getByRole("link", { name: "Mes ami-e-s" });
    expect(friendsLink).toBeInTheDocument();
    expect(friendsLink).toHaveAttribute("href", "/friends");
  });

  it("displays a 'Ma bibliothèque' link", () => {
    const navbarMenu = navbar.getByRole("list", { name: "Menu de Boukin" });
    expect(
      within(navbarMenu).getByRole("link", { name: "Ma bibliothèque" }),
    ).toBeInTheDocument();
  });
  it("displays a 'Mon compte' link", () => {
    const accountLink = navbar.getByRole("link", { name: "Mon compte" });
    expect(accountLink).toBeInTheDocument();
    expect(accountLink).toHaveAttribute("href", "/account");
  });
  it("displays a 'Tableau de bord' link", () => {
    expect(
      navbar.getByRole("link", { name: "Tableau de bord" }),
    ).toBeInTheDocument();
  });
  it("displays a 'Boukin' logo", () => {
    const alt_description_for_logo = "Logo de Boukin";
    expect(
      navbar.getByRole("img", { name: alt_description_for_logo }),
    ).toBeInTheDocument();
  });
});

describe("mobile navbar component test suite", () => {
  let mobileNavbar: ReturnType<typeof within>;
  beforeEach(() => {
    render(NavbarComponent);
    mobileNavbar = within(screen.getByLabelText("Barre de navigation mobile"));
  });
  afterEach(() => cleanup());

  it("displays an icon for 'Mes prêts' for responsive rendering", () => {
    expect(
      mobileNavbar.getByRole("img", { name: "Mes prêts" }),
    ).toBeInTheDocument();
  });
  it("displays an icon for 'Mes ami-e-s' for responsive rendering", () => {
    const friendsIcon = mobileNavbar.getByRole("img", { name: "Mes ami-e-s" });
    const friendsLink = mobileNavbar.getByRole("link", { name: "Mes ami-e-s" });
    expect(friendsIcon).toBeInTheDocument();
    expect(friendsLink).toBeInTheDocument();
    expect(friendsLink).toHaveAttribute("href", "/friends");
  });
  it("displays an icon for 'Ma bibliothèque' for responsive rendering", () => {
    expect(
      mobileNavbar.getByRole("img", { name: "Ma bibliothèque" }),
    ).toBeInTheDocument();
  });
  it("displays an icon for 'Mon compte' for responsive rendering", () => {
    const accountIcon = mobileNavbar.getByRole("img", { name: "Mon compte" });
    const accountLink = mobileNavbar.getByRole("link", { name: "Mon compte" });
    expect(accountIcon).toBeInTheDocument();
    expect(accountLink).toBeInTheDocument();
    expect(accountLink).toHaveAttribute("href", "/account");
  });
  it("displays an icon for 'Tableau de bord' for responsive rendering", () => {
    expect(
      mobileNavbar.getByRole("img", { name: "Tableau de bord" }),
    ).toBeInTheDocument();
  });
});
