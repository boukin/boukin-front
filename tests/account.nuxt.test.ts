import { cleanup, screen } from "@testing-library/vue";
import { afterEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../pages/account.vue";
import {
  mockNuxtImport,
  registerEndpoint,
  renderSuspended,
} from "@nuxt/test-utils/runtime";
import type { AuthenticationToken } from "~/types/authentication";
import { userEvent } from "@testing-library/user-event";

expect.extend(matchers);

const authenticatedUser: AuthenticationToken = {
  username: "Camilledu91",
  email: "camilledu91@hotmail.fr",
  user_id: "3fa85f64-5717-4562-b3fc-2c963f66afa7",
  token: "9876543210",
};
const userCookie = ref(authenticatedUser);

const { updateUserMock, useCookieMock, navigateToMock } = vi.hoisted(() => {
  return {
    updateUserMock: vi.fn(),
    useCookieMock: vi.fn().mockImplementation(() => {
      return userCookie;
    }),
    navigateToMock: vi.fn(),
  };
});
mockNuxtImport("useCookie", () => {
  return useCookieMock;
});
mockNuxtImport("navigateTo", () => {
  return navigateToMock;
});

describe("Account page test suite", () => {
  afterEach(() => {
    cleanup();
    vi.clearAllMocks();
  });

  registerEndpoint(`/users/${authenticatedUser.user_id}/`, updateUserMock);
  it("displays a 'Mon compte' and 'change my account infomations' headings ", async () => {
    await renderSuspended(Component);
    const myAccount = screen.getByRole("heading", {
      name: "Mon compte",
    });
    const changeMyAccount = screen.getByRole("heading", {
      name: "Modifier mes informations",
    });
    expect(myAccount).toBeInTheDocument();
    expect(changeMyAccount).toBeInTheDocument();
  });

  it("displays a form to update username, email and password", async () => {
    await renderSuspended(Component);
    const usernameForm = screen.getByRole("form", {
      name: "Formulaire de modification des informations du compte",
    });
    const usernameInput = screen.getByRole("textbox", {
      name: "Modifier mon nom d'utilisateur",
    });
    const emailInput = screen.getByRole("textbox", {
      name: "Modifier mon addresse e-mail",
    });
    const passwordInput = screen.getByLabelText("Modifier mon mot de passe");
    const button = screen.getByRole("button", {
      name: "Modifier mes informations",
    });
    expect(usernameForm).toBeInTheDocument();
    expect(usernameForm).toContainElement(usernameInput);
    expect(usernameForm).toContainElement(emailInput);
    expect(usernameForm).toContainElement(passwordInput);
    expect(usernameForm).toContainElement(button);
    expect(button).toHaveAttribute("type", "submit");
    expect(usernameInput).toHaveAttribute(
      "placeholder",
      authenticatedUser.username,
    );
    expect(emailInput).toHaveAttribute("placeholder", authenticatedUser.email);
  });

  it("displays a 'account updated' message when successful PATCH request", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);
    updateUserMock.mockResolvedValue(new Response(null, { status: 204 }));

    const usernameInput = screen.getByRole("textbox", {
      name: "Modifier mon nom d'utilisateur",
    });
    expect(
      screen.queryByText("Modification effectuée."),
    ).not.toBeInTheDocument();
    await user.type(usernameInput, "new-username");
    const submitButton = screen.getByRole("button", {
      name: "Modifier mes informations",
    });
    await user.click(submitButton);
    const successMessage = screen.getByText("Modification effectuée.");
    expect(successMessage).toBeInTheDocument();
  });

  it("displays an error message when failed PATCH request", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);
    updateUserMock.mockResolvedValue(new Response(null, { status: 404 }));

    const usernameInput = screen.getByRole("textbox", {
      name: "Modifier mon nom d'utilisateur",
    });
    expect(
      screen.queryByText("Modification effectuée."),
    ).not.toBeInTheDocument();
    await user.type(usernameInput, "new-username");
    const submitButton = screen.getByRole("button", {
      name: "Modifier mes informations",
    });
    await user.click(submitButton);
    const errorMessage = screen.getByText(/Modification impossible/i);
    expect(errorMessage).toBeInTheDocument();
  });

  it("doesn't send request if password does not respect regex", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);

    const usernameInput = screen.getByRole("textbox", {
      name: "Modifier mon nom d'utilisateur",
    });
    await user.type(usernameInput, "new-username");

    const passwordInput = screen.getByLabelText("Modifier mon mot de passe");
    await user.type(passwordInput, "invalidpassword");

    const submitButton = screen.getByRole("button", {
      name: "Modifier mes informations",
    });
    await user.click(submitButton);

    expect(updateUserMock).not.toBeCalled();
    const warningText = screen.getByText(
      /Le mot de passe doit respecter les critères suivants/i,
    );
    expect(warningText).toBeInTheDocument();
  });

  it("doesn't send request if form is empty", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);

    const submitButton = screen.getByRole("button", {
      name: "Modifier mes informations",
    });
    await user.click(submitButton);

    expect(updateUserMock).not.toBeCalled();
  });

  it("updates cookie value when request is successful", async () => {
    const user = userEvent.setup();
    const newUsername = "new-username";
    const newEmail = "new-email@mail.com";
    await renderSuspended(Component);
    updateUserMock.mockResolvedValue(new Response(null, { status: 204 }));

    const usernameInput = screen.getByRole("textbox", {
      name: "Modifier mon nom d'utilisateur",
    });
    const emailInput = screen.getByRole("textbox", {
      name: "Modifier mon addresse e-mail",
    });
    await user.type(usernameInput, newUsername);
    await user.type(emailInput, newEmail);
    const submitButton = screen.getByRole("button", {
      name: "Modifier mes informations",
    });
    await user.click(submitButton);
    expect(userCookie.value.username).toBe(newUsername);
    expect(userCookie.value.email).toBe(newEmail);
  });
});

describe("Account page log out feature test suite", () => {
  afterEach(() => {
    userCookie.value = authenticatedUser;
    navigateToMock.mockReset();
    cleanup();
  });

  it("displays a 'Me déconnecter' log out button", async () => {
    await renderSuspended(Component);
    const logOutButton = screen.getByRole("button", {
      name: "Me déconnecter",
    });
    expect(logOutButton).toBeInTheDocument();
  });

  it("logs out user and redirect him/her to login page when user clicks on log out button", async () => {
    const user = userEvent.setup();
    await renderSuspended(Component);
    const logOutButton = screen.getByRole("button", {
      name: "Me déconnecter",
    });
    expect(userCookie.value).toBeDefined();

    await user.click(logOutButton);

    expect(userCookie.value).not.toBeDefined();
    expect(navigateToMock).toHaveBeenCalledWith("/register");
  });
});
