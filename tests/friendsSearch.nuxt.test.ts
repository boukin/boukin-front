import { cleanup, screen } from "@testing-library/vue";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import * as matchers from "@testing-library/jest-dom/matchers";
import Component from "../pages/friends/search.vue";
import {
  renderSuspended,
  registerEndpoint,
  mockNuxtImport,
} from "@nuxt/test-utils/runtime";
import searchUserFakeData from "./fixtures/users-search.json";
import friendshipsFakeData from "./fixtures/friendships.json";
import userEvent from "@testing-library/user-event";
expect.extend(matchers);

const authenticatedUser = {
  email: "Camilledu91@wanadoo.fr",
  user_id: "1234567890",
  token: "9876543210",
  username: "camille",
};

describe("friends search page static test suite", () => {
  beforeEach(async () => {
    await renderSuspended(Component);
  });
  afterEach(() => cleanup());

  it("displays breadcrumbs ", () => {
    const title = screen.getByText(/Ajouter un·e ami·e/i);
    const parentPageLink = screen.getByRole("link", {
      name: /Mes ami·e·s/i,
    });
    expect(title).toBeInTheDocument();
    expect(parentPageLink).toBeInTheDocument();
    expect(parentPageLink).toHaveAttribute("href", "/friends");
  });

  it("displays research label ", () => {
    const label = screen.getByText(/Recherche par e-mail/i);
    expect(label).toBeInTheDocument();
  });

  it("displays a form as a searchbar", () => {
    const searchForm = screen.getByRole("form");
    const input = screen.getByRole("textbox");
    const button = screen.getByRole("button");
    expect(searchForm).toBeInTheDocument();
    expect(input).toBeInTheDocument();
    expect(input).toHaveAttribute("placeholder", "email@exemple.com");
    expect(searchForm).toContainElement(input);
    expect(searchForm).toContainElement(button);
    expect(button).toHaveAttribute("type", "submit");
  });
});

describe("friends search page dynamic test suite", () => {
  const mocks = vi.hoisted(() => {
    return {
      searchUser: vi.fn(),
      fetchFriendships: vi.fn(),
    };
  });

  mocks.fetchFriendships.mockReturnValue(friendshipsFakeData);
  registerEndpoint(
    `/users/${authenticatedUser.user_id}/friendships/`,
    mocks.fetchFriendships,
  );

  const { useCookieMock } = vi.hoisted(() => {
    return {
      useCookieMock: vi.fn().mockImplementation(() => {
        return {
          value: authenticatedUser,
        };
      }),
    };
  });
  mockNuxtImport("useCookie", () => {
    return useCookieMock;
  });

  beforeEach(async () => {
    await renderSuspended(Component);
  });
  afterEach(() => cleanup());

  it("displays usercard with retrieved user's data when successful search", async () => {
    const user = userEvent.setup();
    const retrievedUser = searchUserFakeData.not_friend;
    mocks.searchUser.mockResolvedValue(retrievedUser);
    registerEndpoint(
      `/users/search/?email=${retrievedUser.email}`,
      mocks.searchUser,
    );

    const searchBar = screen.getByRole("textbox", {
      name: "email",
    });
    expect(searchBar).toBeInTheDocument();

    await user.type(searchBar, retrievedUser.email);
    const submitButton = screen.getByRole("button", { name: /rechercher/i });
    expect(submitButton).toBeInTheDocument();

    await user.click(submitButton);
    const retrievedUsername = screen.getByText(retrievedUser.username);
    expect(retrievedUsername).toBeInTheDocument();
  });

  it("displays an active friendship request button when searched user is NOT A FRIEND", async () => {
    const user = userEvent.setup();
    const retrievedUser = searchUserFakeData.not_friend;
    mocks.searchUser.mockResolvedValue(retrievedUser);
    registerEndpoint(
      `/users/search/?email=${retrievedUser.email}`,
      mocks.searchUser,
    );
    const searchBar = screen.getByRole("textbox", {
      name: "email",
    });
    const submitButton = screen.getByRole("button", { name: /rechercher/i });
    await user.type(searchBar, retrievedUser.email);
    await user.click(submitButton);
    const retrievedUsername = screen.getByText(retrievedUser.username);
    const requestButton = screen.getByRole("button", {
      name: "Envoyer une demande",
    });
    expect(retrievedUsername).toBeInTheDocument();
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).not.toBeDisabled();
  });

  const requestSentMessageScenarios = [
    {
      description: "REQUESTING + FRIENDSHIP_REQUESTED",
      retrievedUser: searchUserFakeData.requested_requesting,
    },
    {
      description: "ADDRESSED + FRIENDSHIP_REQUESTED",
      retrievedUser: searchUserFakeData.requested_addressed,
    },
  ];
  requestSentMessageScenarios.forEach(({ description, retrievedUser }) => {
    it(`displays a request sent message when searched user is ${description}`, async () => {
      const user = userEvent.setup();
      mocks.searchUser.mockResolvedValue(retrievedUser);
      registerEndpoint(
        `/users/search/?email=${retrievedUser.email}`,
        mocks.searchUser,
      );
      const searchBar = screen.getByRole("textbox", {
        name: "email",
      });
      const submitButton = screen.getByRole("button", { name: /rechercher/i });
      await user.type(searchBar, retrievedUser.email);
      await user.click(submitButton);
      const retrievedUsername = screen.getByText(retrievedUser.username);
      const requestButton = screen.queryByRole("button", {
        name: "Envoyer une demande",
      });
      const requestSentMessage = screen.getByText(/Demande envoyée !/i);
      expect(retrievedUsername).toBeInTheDocument();
      expect(requestButton).not.toBeInTheDocument();
      expect(requestSentMessage).toBeInTheDocument();
    });
  });
  const alreadyFriendsMessageScenarios = [
    {
      description: "REQUESTING + FRIENDSHIP_ACTIVE",
      retrievedUser: searchUserFakeData.active_requesting,
    },
    {
      description: "ADDRESSED + FRIENDSHIP_ACTIVE",
      retrievedUser: searchUserFakeData.active_addressed,
    },
  ];
  alreadyFriendsMessageScenarios.forEach(({ description, retrievedUser }) => {
    it(`displays an already friends message when searched user is ${description}`, async () => {
      const user = userEvent.setup();
      mocks.searchUser.mockResolvedValue(retrievedUser);
      registerEndpoint(
        `/users/search/?email=${retrievedUser.email}`,
        mocks.searchUser,
      );
      const searchBar = screen.getByRole("textbox", {
        name: "email",
      });
      const submitButton = screen.getByRole("button", { name: /rechercher/i });
      await user.type(searchBar, retrievedUser.email);
      await user.click(submitButton);
      const retrievedUsername = screen.getByText(retrievedUser.username);
      const requestButton = screen.queryByRole("button", {
        name: "Envoyer une demande",
      });
      const alreadyFriendsMessage = screen.getByText(
        /Vous êtes déjà ami·e·s./i,
      );
      expect(retrievedUsername).toBeInTheDocument();
      expect(requestButton).not.toBeInTheDocument();
      expect(alreadyFriendsMessage).toBeInTheDocument();
    });
  });
  const disabledButtonScenarios = [
    {
      description: "ADDRESSED + FRIENDSHIP_DENIED",
      retrievedUser: searchUserFakeData.denied_addressed,
    },
    {
      description: "REQUESTING + FRIENDSHIP_OVER",
      retrievedUser: searchUserFakeData.over_requesting,
    },
  ];
  disabledButtonScenarios.forEach(({ description, retrievedUser }) => {
    it(`displays a disabled friendship request button when searched user is ${description}`, async () => {
      const user = userEvent.setup();
      mocks.searchUser.mockResolvedValue(retrievedUser);
      registerEndpoint(
        `/users/search/?email=${retrievedUser.email}`,
        mocks.searchUser,
      );
      const searchBar = screen.getByRole("textbox", {
        name: "email",
      });
      const submitButton = screen.getByRole("button", { name: /rechercher/i });
      await user.type(searchBar, retrievedUser.email);
      await user.click(submitButton);
      const retrievedUsername = screen.getByText(retrievedUser.username);
      const requestButton = screen.getByRole("button", {
        name: "Envoyer une demande",
      });
      expect(retrievedUsername).toBeInTheDocument();
      expect(requestButton).toBeInTheDocument();
      expect(requestButton).toBeDisabled();
    });
  });
  const enabledButtonScenarios = [
    {
      description: "NOT A FRIEND",
      retrievedUser: searchUserFakeData.not_friend,
    },
    {
      description: "ADDRESSED + FRIENDSHIP_OVER",
      retrievedUser: searchUserFakeData.over_addressed,
    },
    {
      description: "REQUESTING + FRIENDSHIP_DENIED",
      retrievedUser: searchUserFakeData.denied_requesting,
    },
  ];
  enabledButtonScenarios.forEach(({ description, retrievedUser }) => {
    it(`displays an enabled friendship request button when searched user is ${description}`, async () => {
      const user = userEvent.setup();
      mocks.searchUser.mockResolvedValue(retrievedUser);
      registerEndpoint(
        `/users/search/?email=${retrievedUser.email}`,
        mocks.searchUser,
      );
      const searchBar = screen.getByRole("textbox", {
        name: "email",
      });
      const submitButton = screen.getByRole("button", { name: /rechercher/i });
      await user.type(searchBar, retrievedUser.email);
      await user.click(submitButton);
      const retrievedUsername = screen.getByText(retrievedUser.username);
      const requestButton = screen.getByRole("button", {
        name: "Envoyer une demande",
      });
      expect(retrievedUsername).toBeInTheDocument();
      expect(requestButton).toBeInTheDocument();
      expect(requestButton).not.toBeDisabled();
    });
  });
  it("displays an error message if no user retrieved", async () => {
    const user = userEvent.setup();
    const unknownUserEmail = "unknown.user@mail.com";
    const apiErrorResponse = {
      value: { message: "Error message" },
    };
    mocks.searchUser.mockRejectedValue(apiErrorResponse);
    registerEndpoint(
      `/users/search/?email=${unknownUserEmail}`,
      mocks.searchUser,
    );
    const searchBar = screen.getByRole("textbox", {
      name: "email",
    });
    const submitButton = screen.getByRole("button", { name: /rechercher/i });
    await user.type(searchBar, unknownUserEmail);
    await user.click(submitButton);
    const retrievedUsername = screen.queryByText(unknownUserEmail);
    const requestButton = screen.queryByRole("button", {
      name: "Envoyer une demande",
    });
    const errorMessage = screen.getByText(
      /Nous n’avons pas trouvé d’utilisateur·ice correspondant à votre recherche./i,
    );
    expect(retrievedUsername).not.toBeInTheDocument();
    expect(requestButton).not.toBeInTheDocument();
    expect(errorMessage).toBeInTheDocument();
  });

  it("displays a disabled friendship request button when searched user is authenticated user", async () => {
    const user = userEvent.setup();
    const retrievedUser = {
      user_id: authenticatedUser.user_id,
      username: "Camille",
      email: authenticatedUser.email,
    };
    mocks.searchUser.mockResolvedValue(retrievedUser);
    registerEndpoint(
      `/users/search/?email=${retrievedUser.email}`,
      mocks.searchUser,
    );
    const searchBar = screen.getByRole("textbox", {
      name: "email",
    });
    const submitButton = screen.getByRole("button", { name: /rechercher/i });
    await user.type(searchBar, retrievedUser.email);
    await user.click(submitButton);
    const requestButton = screen.getByRole("button", {
      name: "Envoyer une demande",
    });
    expect(requestButton).toBeInTheDocument();
    expect(requestButton).toBeDisabled();
  });

  it("allows user doing several searches in a row", async () => {
    const user = userEvent.setup();
    const unknownUserEmail = "unknown.user@mail.com";
    const existingUser = searchUserFakeData.not_friend;

    // 1ère recherche qui renvoit une erreur
    const apiResponse = new Response(null, { status: 404 });
    mocks.searchUser.mockResolvedValue(apiResponse);
    registerEndpoint(
      `/users/search/?email=${unknownUserEmail}`,
      mocks.searchUser,
    );
    const searchBar = screen.getByRole("textbox", {
      name: "email",
    });
    const submitButton = screen.getByRole("button", { name: /rechercher/i });
    await user.type(searchBar, unknownUserEmail);
    await user.click(submitButton);
    const errorMessage = screen.getByText(
      /Nous n’avons pas trouvé d’utilisateur·ice correspondant à votre recherche./i,
    );
    expect(errorMessage).toBeInTheDocument();

    // 2ème recherche qui fonctionne
    mocks.searchUser.mockResolvedValue(existingUser);
    registerEndpoint(
      `/users/search/?email=${existingUser.email}`,
      mocks.searchUser,
    );
    await user.clear(searchBar);
    await user.type(searchBar, existingUser.email);
    await user.click(submitButton);
    const retrievedUsername = screen.getByText(existingUser.username);
    expect(retrievedUsername).toBeInTheDocument();
  });
});
