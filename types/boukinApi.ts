export type User = {
  user_id: string;
  username: string;
  email: string;
  password: string;
  role: string;
  created_at: string;
  updated_at: string;
};

export type Book = {
  book_id: string;
  title: string;
  author_fname: string;
  author_lname: string;
  isbn: string;
  created_at: string;
  updated_at: string;
};

export type BookCreation = Omit<Book, "book_id" | "created_at" | "updated_at">;

export type OwnedBook = {
  owned_book_id: string;
  owner: User;
  book: Book;
  status: "available" | "loaned" | "not visible";
};
