import type { User } from "./user";
export interface Friendship {
  /** ID of the friendship */
  friendship_id: string;
  /** Status of the friendship */
  status:
    | "friendship_requested"
    | "friendship_active"
    | "friendship_denied"
    | "friendship_over";
  /** Friend details without password + isRequestingUser */
  friend: Omit<User, "password"> & { is_requesting_user: boolean };
}
