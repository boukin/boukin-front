export type User = {
  /** ID of the user */
  user_id: string;
  /** Username of the user */
  username: string;
  /** E-mail of the user */
  email: string;
  /** Password of the user */
  password: string;
  /** Role of the user */
  role: "user" | "admin" | "owner" | "external";
};
