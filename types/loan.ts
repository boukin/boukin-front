import type { User } from "./user";
import type { Book, OwnedBook } from "./boukinApi";

export interface Loan {
  loan_id: string;
  owner: Omit<User, "email" | "password" | "role">;
  borrower: Omit<User, "email" | "password" | "role">;
  owned_book: LoanOwnedBook;
  status: LoanStatus;
  starting_date: string;
  ending_date: string;
  created_at: string;
  updated_at: string;
}

export interface LoanOwnedBook {
  owned_book_id: string;
  owner: Omit<
    User,
    "email" | "password" | "role" | "updated_at" | "created_at"
  >;
  book: Omit<Book, "book_id" | "created_at" | "updated_at">;
}

export const enum LoanUpdateType {
  LoanRequest = "loan_request",
  BookReturn = "book_return",
  AskingBookReturn = "asking_book_return",
  ReturningLoan = "returning_loan",
}

export const enum LoanStatus {
  Active = "loan_active",
  Denied = "loan_denied",
  Requested = "loan_requested",
  ReturnRequested = "return_requested",
  ReturnConfirmationPending = "return_confirmation_pending",
  ReturnConfirmed = "return_confirmed",
}

export const enum ButtonText {
  LoanRequest = "Accepter / Refuser",
  BookReturn = "Confirmer",
  AskingBookReturn = "← Réclamer",
  ReturningLoan = "→ Rendre",
  AcceptLoanRequest = "Accepter",
  DenyLoanRequest = "Refuser",
}

export const enum LoanUpdateMessage {
  Active = "Le prêt est désormais actif.",
  Denied = "Vous avez refusé cette demande de prêt.",
  ReturnRequested = "Vous avez demandé le retour de votre livre prêté.",
  ReturnConfirmationPending = "Vous avez déclaré avoir rendu ce livre emprunté.",
  ReturnConfirmed = "Vous avez confirmé le retour du livre que vous aviez prêté.",
}
