export type UserAuthentication = {
  email: string;
  password: string;
};

export type UserRegistration = UserAuthentication & {
  username: string;
  passwordConfirmation: string;
};

export interface AuthenticationToken {
  user_id: string;
  email: string;
  token: string;
  username: string;
}
